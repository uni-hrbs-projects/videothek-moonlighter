using System;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class ScoreBasedPlaytime : MonoBehaviour
{
    public static ScoreBasedPlaytime instance;
    
    [SerializeField] private IntergerSo gameVersion;
    [SerializeField] private IntergerSo score3D;
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private UnityEvent onTimeOver;

    private int onDeathPenalty = 50;
    private float currentScore;
    private bool timeOver;
    private TrackScore _trackScore;
    public String LevelName;

    private void Awake()
    { 
        Debug.Log("Game Version is: " + gameVersion);
        instance = this;
        
        _trackScore = FindObjectOfType<TrackScore>();
        currentScore = score3D.integer;
        scoreText.text = Convert.ToInt32(currentScore) + "";
        
        if(_trackScore!=null)
            _trackScore.Log("Started Level " + LevelName + " with " + currentScore + " Dollar!");
    }

    private void Update()
    {
        if (gameVersion.integer != 0)
            return;
        
        if (timeOver)
            return;
        
        currentScore -= Time.unscaledDeltaTime;
        
        MoneyCheck();
    }

    public void OnDeathReduction()
    {
        if (gameVersion.integer != 1)
            return;

        currentScore -= onDeathPenalty;
        
        MoneyCheck();
    }

    private void MoneyCheck()
    {
        if (currentScore <= 0)
        {
            currentScore = 0;
            timeOver = true;
            onTimeOver?.Invoke();
        }        
        
        scoreText.text = Convert.ToInt32(currentScore) + "";
    }

    private void OnDestroy()
    {
        score3D.integer = Convert.ToInt32(currentScore);
        instance = null;
    }
}
