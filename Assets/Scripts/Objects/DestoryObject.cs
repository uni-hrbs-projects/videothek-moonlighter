using UnityEngine;

public class DestoryObject : MonoBehaviour
{
    public void DestroyMe()
    {
        Destroy(gameObject);
    }
}
