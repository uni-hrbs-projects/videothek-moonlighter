using System;
using UnityEngine;

public class GameMaster : MonoBehaviour
{
    public static GameMaster instance;
    public Vector2 lastCheckPointPosition;
    public float batteryValue;
    public int checkpointNumber = 0;
    public float startTime;
    public bool coinMultiplier = false;
    private int _repawnCountOnCheckpoint = 0;

    public delegate void EventHandler(object sender, EventArgs args);
    public event EventHandler CheckpointSet = delegate{};
    public void onSetCheckpoint() => CheckpointSet(this, new EventArgs());

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
            startTime = Time.time;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void checkCoinMultiplier()
    {
        _repawnCountOnCheckpoint++;
        if (_repawnCountOnCheckpoint>2)
        {
            coinMultiplier = true;
        }
    }

    public void setCheckpoint(Vector2 position, int checkpointNumber, float batteryCharge)
    {
        if (checkpointNumber != this.checkpointNumber)
        {
            _repawnCountOnCheckpoint = 0;
            coinMultiplier = false;
        }
        this.lastCheckPointPosition = position;
        this.checkpointNumber = checkpointNumber;
        this.batteryValue = batteryCharge;
        onSetCheckpoint();
    }

    public void unsubscribeListeners()
    {
        Delegate[] listenerArray = CheckpointSet.GetInvocationList();
        foreach (Delegate listener in listenerArray)
        {
            CheckpointSet -= (listener as EventHandler);
        }
    }

    public void ResetCheckpoint()
    {
        lastCheckPointPosition = Vector2.zero;
        checkpointNumber = 0;
        startTime = Time.time;
        _repawnCountOnCheckpoint = 0;
        unsubscribeListeners();
    }
}
