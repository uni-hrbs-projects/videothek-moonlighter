using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button_Sprite : MonoBehaviour
{
    public SpriteRenderer headparent;
    public GameObject[] btn_head=new GameObject[2];
    void Start()
    {
        Init_Color();
    }
    void Init_Color()
    {
        Color color = headparent.color;
        foreach (GameObject btn in btn_head)
        {
            btn.gameObject.GetComponent<SpriteRenderer>().color = color;
        }
    }
    public void Activate()
    {
        btn_head[0].active = false;
        btn_head[1].active = true;
    }
    public void Deactivate()
    {
        btn_head[0].active = true;
        btn_head[1].active = false;
    }

}
