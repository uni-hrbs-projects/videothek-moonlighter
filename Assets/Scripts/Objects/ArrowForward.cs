using System;
using UnityEngine;

public class ArrowForward : MonoBehaviour
{
    [SerializeField] private Transform targetDirection;
    [SerializeField] private float duration = 1.5f;
    [SerializeField] private float speed = 10;
    [HideInInspector] public bool active = true;
    
    private float timeGoneby = 0;
    private bool lastActive = true;
    private Vector3 direction;

    private void Start()
    {
        direction = targetDirection.position - transform.position;
        direction = direction.normalized;
    }

    // Update is called once per frame
    private void Update()
    {
        if (TimeBodyController.allRewind)
        {
            // Dont move forward, and subtract rewind time from active duration
            timeGoneby -= Time.deltaTime;
        }
        
        active = !(timeGoneby > duration); // Object should only be active and moving for the duration length, the rest is just for possible rewind
        if (active != lastActive) // Adjust the child Objects activity State in case there was an active-state-change
        {
            foreach (Transform child in transform)
            {
                child.gameObject.SetActive(active);
            }
        }

        if (TimeBodyController.allRewind)
            return;
        
        if (timeGoneby > duration + TimeBodyController.recordTime)
        {
            // Dispose off this gameobject if max rewind time exceeds this objects lifetime
            Destroy(gameObject);
        }

        transform.Translate(direction * speed * Time.deltaTime);
        timeGoneby += Time.deltaTime;
        lastActive = active;
    }
    
    private void OnDrawGizmos(){
        Gizmos.DrawLine(transform.position, targetDirection.position);
    }
}
