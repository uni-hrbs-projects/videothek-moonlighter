using UnityEngine;

public class Rotating : MonoBehaviour
{
    [SerializeField] private float rotSpeed;
    [SerializeField] private bool yAxis;

    private void FixedUpdate()
    {
        // Dont rotate during rewind, so we can rewind the rotation
        if (TimeBodyController.allRewind)
            return;
        
        if(yAxis)
            transform.Rotate(new Vector3(0, rotSpeed * Time.fixedDeltaTime, 0));
        else
            transform.Rotate(new Vector3(0, 0, rotSpeed * Time.fixedDeltaTime));
    }
}
