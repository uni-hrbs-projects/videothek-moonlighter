using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rewindable : MonoBehaviour
{
    [SerializeField] private List<AbstractTimeTracker> trackedAttributes = new List<AbstractTimeTracker>();
    [SerializeField] private bool isRigidbody;

    private bool isRewinding;
    private Rigidbody2D _rigidbody2D;

    private void Start()
    {
        if (isRigidbody)
            _rigidbody2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update() 
    {
        if (TimeBodyController.allRewind & !isRewinding)
        {
            StartRewind();
        }		
		
        if (!TimeBodyController.allRewind & isRewinding)
        {
            StopRewind();
        }	
    }
    
    private void StartRewind()
    {
        if (isRewinding)
            return;

        if (isRigidbody)
            _rigidbody2D.isKinematic = true;
        
        isRewinding = true;
    }

    private void StopRewind()
    {
        if (!isRewinding)
            return;
        
        if (isRigidbody)
            _rigidbody2D.isKinematic = false;
		
        isRewinding = false;
    }
    
    void FixedUpdate()
    {
        if (isRewinding)
        {
            if(isRigidbody)
                _rigidbody2D.velocity = Vector2.zero;
            
            foreach (var attribute in trackedAttributes)
            {
                attribute.Rewind();
            }
        }
        else
        {
            foreach (var attribute in trackedAttributes)
            {
                attribute.Record();
            }
        }
    }
}
