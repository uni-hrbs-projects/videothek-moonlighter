using System.Collections.Generic;
using UnityEngine;

public class TrackedBreakingState : AbstractTimeTracker
{
    private List<BreakingPlatform.breakingState> pointInTime = new List<BreakingPlatform.breakingState>();
    private BreakingPlatform _breakingPlatform;

    private void Start()
    {
        _breakingPlatform = GetComponent<BreakingPlatform>();
    }

    public override void Record()
    {
        if (pointInTime.Count > Mathf.Round(TimeBodyController.recordTime / Time.fixedDeltaTime))
            pointInTime.RemoveAt(pointInTime.Count - 1);
        
        pointInTime.Insert(0, _breakingPlatform._breakingState);
    }

    public override void Rewind()
    {
        if (pointInTime.Count > 0)
        {
            if(_breakingPlatform._breakingState != pointInTime[0])
            {
                _breakingPlatform._breakingState = pointInTime[0];
                _breakingPlatform.ChangePlatformState(pointInTime[0]);
            }            
            pointInTime.RemoveAt(0);
        }
    }
}
