using System.Collections.Generic;
using UnityEngine;

public class TrackedRotation : AbstractTimeTracker
{
    private List<Quaternion> pointInTime = new List<Quaternion>();
    
    public override void Record()
    {
        if (pointInTime.Count > Mathf.Round(TimeBodyController.recordTime / Time.fixedDeltaTime))
            pointInTime.RemoveAt(pointInTime.Count - 1);
        
        pointInTime.Insert(0, transform.rotation);
    }

    public override void Rewind()
    {
        if (pointInTime.Count > 0)
        {
            transform.rotation = pointInTime[0];
            pointInTime.RemoveAt(0);
        }
    }
}
