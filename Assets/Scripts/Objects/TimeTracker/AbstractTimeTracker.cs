using UnityEngine;

public abstract class AbstractTimeTracker: MonoBehaviour
{
    public abstract void Record();
    public abstract void Rewind();
}
