using System.Collections.Generic;
using UnityEngine;

public class TrackedButtonState : AbstractTimeTracker
{
    private List<bool> pointInTime = new List<bool>();
    private ButtonInteraction _buttonInteraction;

    private void Start()
    {
        _buttonInteraction = GetComponent<ButtonInteraction>();
    }

    public override void Record()
    {
        if (pointInTime.Count > Mathf.Round(TimeBodyController.recordTime / Time.fixedDeltaTime))
            pointInTime.RemoveAt(pointInTime.Count - 1);
        
        pointInTime.Insert(0, _buttonInteraction.isTriggered);
    }

    public override void Rewind()
    {
        if (pointInTime.Count > 0)
        {
            if(_buttonInteraction.isTriggered != pointInTime[0])
            {
                _buttonInteraction.isTriggered = pointInTime[0];
                _buttonInteraction.ChangeButtonState();
            }            
            pointInTime.RemoveAt(0);
        }
    }
}
