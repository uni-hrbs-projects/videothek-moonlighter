using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TrackedPosition : AbstractTimeTracker
{
    private List<Vector3> pointInTime = new List<Vector3>();
    [SerializeField] private UnityEvent OnNoPointsLeftEvent;
    
    public override void Record()
    {
        if (pointInTime.Count > Mathf.Round(TimeBodyController.recordTime / Time.fixedDeltaTime))
            pointInTime.RemoveAt(pointInTime.Count - 1);
        
        pointInTime.Insert(0, transform.position);
    }

    public override void Rewind()
    {
        if (pointInTime.Count > 0)
        {
            transform.position = pointInTime[0];
            pointInTime.RemoveAt(0);
        }
        else
        {
            OnNoPointsLeftEvent?.Invoke();
        }
    }
}
