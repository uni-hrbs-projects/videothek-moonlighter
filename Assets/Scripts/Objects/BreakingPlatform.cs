using UnityEngine;

public class BreakingPlatform : MonoBehaviour
{
    // TODO: Time Tracking for this Object!
    
    public enum breakingState
    {
        notBroken,
        isbreaking,
        Broke
    };
    
    [SerializeField] private float breakTime = 0.3f;
    [HideInInspector] public breakingState _breakingState = breakingState.notBroken;
    private Rigidbody2D rb;
    private float brokenSince = 0;
    private SpriteRenderer platformSprite;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        platformSprite = GetComponent<SpriteRenderer>();
    }

    private void OnCollisionStay2D(Collision2D other)
    {
        if (!other.transform.CompareTag("Character") || TimeBodyController.allRewind)
            return;

        if (_breakingState == breakingState.notBroken)
            _breakingState = breakingState.isbreaking;
    }
    
    private void Update()
    {
        if (TimeBodyController.allRewind)
        {
            brokenSince -= Time.deltaTime * 2; // Subtract the deltaTime twice, as we let the loop run to adjust the color of the breaking platform, but it also adds the deltaTime to it again.
            if (brokenSince < 0)
                brokenSince = 0;
        }

        switch (_breakingState)
        {
            case breakingState.isbreaking:
                brokenSince += Time.deltaTime; // Count Timer Until breaking
                AdjustPlatform();
                if (brokenSince >= breakTime)
                {
                    _breakingState = breakingState.Broke;
                    ChangePlatformState(_breakingState);
                }
                break;
            case breakingState.Broke:
                brokenSince += Time.deltaTime;
                // Can dispose of the object now, as rewinding will not reach this anymore
                if(brokenSince > TimeBodyController.recordTime)
                    Destroy(gameObject.transform.parent.gameObject);
                break;
        }
    }

    public void ChangePlatformState(breakingState breakingState)
    {
        brokenSince = 0; // Reset Timer
        
        switch (breakingState)
        {
            case breakingState.notBroken:
                rb.constraints = RigidbodyConstraints2D.FreezeAll;
                break;
            case breakingState.isbreaking:
                break;
            case breakingState.Broke:
                rb.constraints = RigidbodyConstraints2D.None;
                break;
        }
    }

    private void AdjustPlatform()
    {
        // This adjust the platform visually to indicate the state of breaking
        float percentageBroken = Mathf.Clamp01(brokenSince / breakTime);
        Color newColor = Color.Lerp(Color.white, Color.red, percentageBroken);
        platformSprite.color = newColor;
    }
}
