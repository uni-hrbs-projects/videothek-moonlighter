using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resetable : MonoBehaviour
{
    List<GameObject> resetables = new List<GameObject>();

    public void Add_Item(Consumerable item)
    {
        resetables.Add(item.gameObject);
    }
    public void ResetAll()
    {
        foreach(GameObject item in resetables)
        {
            item.SetActive(true);
        }
    }

}
