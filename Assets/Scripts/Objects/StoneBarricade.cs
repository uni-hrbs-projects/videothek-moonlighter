using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneBarricade : MonoBehaviour
{
    [Range(0f,0.1f)]
    public float speed;
    Vector2 startpos;
    public Transform targetpos;
    [Header("ParticlePlay")]
    public ParticlePlay pp;

    private void Start()
    {
        startpos = transform.localPosition;
    }
    public void Activate()
    {
        LeanTween.move(gameObject, targetpos,speed).setEase(LeanTweenType.easeInElastic);
    }
    public void Deactivate()
    {
        LeanTween.move(gameObject,startpos, speed ).setEase(LeanTweenType.easeInElastic); ;
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(transform.position, targetpos.position);
    }

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Projectile"))
        {
            collision.GetComponentInParent<DestoryObject>().DestroyMe();
            pp._particleSystem.transform.position = collision.transform.position;
            pp.PlayParticle();
        }
    }


}
