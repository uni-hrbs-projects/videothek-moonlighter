using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Serialization;

public class CheckPoint : MonoBehaviour 
{
    // have we been triggered?
    bool triggered;
    private GameMaster gameMaster;
    private BatteryManager battery;
    public int CheckpointNumber;
    public TrackTime _trackTime;

    private void Start()
    {
        gameMaster = GameObject.FindGameObjectWithTag("GameMaster").GetComponent<GameMaster>();
        battery = GameObject.FindGameObjectWithTag("Battery").GetComponent<BatteryManager>();
        _trackTime = FindObjectOfType<TrackTime>();
    }

    void Awake()
    {
        triggered = false;
    }
    // called whenever another collider enters our zone (if layers match)
    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.layer 
            == LayerMask.NameToLayer("Character"))
        {
            if ( ! triggered)
            {
                Trigger();
                gameMaster.setCheckpoint(transform.position, CheckpointNumber, battery.GetBatteryCharge());
                if(_trackTime!=null)
                    _trackTime.Log("Triggered Checkpoint " + CheckpointNumber + " after " + (Time.time - gameMaster.startTime));
            }
        }
    }
    void Trigger()
    {
        transform.GetChild(0).GetComponent<SpriteRenderer>().color = Color.green;
        triggered = true;
    }
}