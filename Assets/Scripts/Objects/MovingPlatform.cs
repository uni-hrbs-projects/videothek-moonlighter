using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public Transform pos1, pos2;
    public float speed;
    public Transform startPos;
    [SerializeField] private bool active;
    private Vector3 nextPos;
    bool btn_triggered=false;
    
    // Start is called before the first frame update
    void Start()
    {
        nextPos = startPos.position;
    }
    public void Activate()
    {
        btn_triggered = true;
    }
    public void DeActivation()
    {
        btn_triggered = false;
    }

    // Update is called once per frame
    void Update()
    {
        // Dont Move during rewind, so we can rewind the rotation
        if (TimeBodyController.allRewind)
            return;
        
        if(active)
        {
            if(transform.position==pos1.position)
            {
                nextPos = pos2.position;
            }
            
            if(transform.position==pos2.position)
            {
                nextPos = pos1.position;
            }
            
            transform.position = Vector3.MoveTowards(transform.position, nextPos, speed*Time.deltaTime);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.tag=="Character" && btn_triggered)
        {
            active = true;
        }
    }

    private void OnDrawGizmos(){
        Gizmos.DrawLine(pos1.position, pos2.position);
    }
}
