using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item_Battery : Consumerable
{
    public float chargeAmount = 100;
    BatteryManager baMan;

    void Start()
    {
        baMan = GameObject.FindObjectOfType<BatteryManager>().GetComponent<BatteryManager>();
    }
    protected override void Affect()
    {
        baMan.Charge_Battery(chargeAmount);
    }


}
