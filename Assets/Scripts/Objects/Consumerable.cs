using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Consumerable : MonoBehaviour
{
    public enum option{none,reset,destroy}
    public option disposalOption;

    [SerializeField] Resetable resetManager;

    protected void Disposal()
    {
        switch (disposalOption)
        {
            case option.destroy:
                Object.Destroy(gameObject);
                break;
            case option.reset:
                resetManager.Add_Item(this);
                gameObject.SetActive(false);
                break;
        }
    }
    protected virtual void Affect()
    {

    }
    protected  void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Character")
        {
            Affect();
            Disposal();
        }
    }
}
