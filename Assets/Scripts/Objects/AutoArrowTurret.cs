using System.Collections;
using UnityEngine;

public class AutoArrowTurret : MonoBehaviour
{
    [SerializeField]private SpawnPrefab spawnPrefab;
    [SerializeField] private float delay;
    void Start()
    {
        StartCoroutine(Delay());
    }

    private IEnumerator Delay()
    {
        yield return new WaitForSeconds(delay);
        spawnPrefab.InvokeRepeating("Spawn", 0,1.5f);
    }
}
