using System;
using UnityEngine;

public class ButtonAnimation : MonoBehaviour
{
    [SerializeField] private Transform moveButton;
    [SerializeField] private float aniamtionTime = 0.1f;
    [SerializeField] private float animationTargetPos = 0;
    private float startPos;

    private void Start()
    {
        startPos = moveButton.localPosition.y;
    }

    public void AnimateButtonActivation()
    {
        LeanTween.moveLocalY(moveButton.gameObject, animationTargetPos, aniamtionTime);
            //.setOnComplete(ChangeColorGreen);
    }

    public void AnimateButtonDeactivation()
    {
        LeanTween.moveLocalY(moveButton.gameObject, startPos, aniamtionTime);
            //.setOnComplete(ChangeColorRed);
    }

    //private void ChangeColorGreen()
    //{
    //    moveButton.GetComponent<SpriteRenderer>().color = Color.green;
    //}
    
    //private void ChangeColorRed()
    //{
    //    moveButton.GetComponent<SpriteRenderer>().color = Color.white;
    //}
}
