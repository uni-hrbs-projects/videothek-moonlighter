using UnityEngine;

public class SetSceneToLoad : MonoBehaviour
{
    [SerializeField] private IntergerSo toLoad;
    [SerializeField] private LoadScene loadScene;

    private void Awake()
    {
        loadScene.sceneToLoadInt = toLoad.integer;
    }
}
