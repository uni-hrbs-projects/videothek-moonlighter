using UnityEngine;

public class SpawnPrefab : MonoBehaviour
{
    [SerializeField] private GameObject prefabToSpawn;

    public void Spawn()
    {
        Instantiate(prefabToSpawn, transform.position, Quaternion.identity);
    }
}
