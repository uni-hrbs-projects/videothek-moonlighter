using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveObjects : MonoBehaviour
{
    public List<GameObject> objects;

    public void Active()
    {
        foreach(GameObject obj in objects)
        {
            obj.SetActive(true);
        }

    }

}
