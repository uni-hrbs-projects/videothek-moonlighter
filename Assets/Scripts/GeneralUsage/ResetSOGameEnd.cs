using System.Collections.Generic;
using UnityEngine;

public class ResetSOGameEnd : MonoBehaviour
{

    [System.Serializable]
    private struct BoolSoSetter
    {
        public BoolSo BoolSo;
        public bool startValue;
    }
    [SerializeField] private List<BoolSoSetter> boolObjects = new List<BoolSoSetter>();

    [System.Serializable]
    private struct IntegerSoSetter
    {
        public IntergerSo IntSo;
        public int startValue;
    }
    [SerializeField] private List<IntegerSoSetter> integerObjects = new List<IntegerSoSetter>();

    [System.Serializable]
    private struct FloatSoSetter
    {
        public FloatSo FloatSo;
        public float startValue;
    }
    [SerializeField] private List<FloatSoSetter> floatObjects = new List<FloatSoSetter>();

    [SerializeField] private Input2D _input2D;
    [SerializeField] private Input3D input3D;


    private void Awake()
    {
        foreach (var bo in boolObjects)
            bo.BoolSo.boolean = bo.startValue;

        foreach (var io in integerObjects)
            io.IntSo.integer = io.startValue;
        
        foreach (var fo in floatObjects)
            fo.FloatSo.FloatValue = fo.startValue;
        
        _input2D.ResetInput();
        input3D.ResetInput();
    }
}
