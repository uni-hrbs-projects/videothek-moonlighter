using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    [SerializeField] private bool useBuildIndex;
    public int sceneToLoadInt;
    [SerializeField] private string sceneToLoad;
    
    public void CallScene()
    {
        if(useBuildIndex)
            SceneManager.LoadScene(sceneToLoadInt);
        else
            SceneManager.LoadScene(sceneToLoad);
    }
}
