using UnityEngine;
using UnityEngine.SceneManagement;

public class SetActiveScene : MonoBehaviour
{
    [SerializeField] private string sceneName;

    private void Start()
    {
        SceneManager.SetActiveScene(SceneManager.GetSceneByName(sceneName));
    }
}
