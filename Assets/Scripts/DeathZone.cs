using System;
using UnityEngine;

public class DeathZone : MonoBehaviour
{
    [SerializeField] private BoolSo isRewindingCharacter;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.transform.CompareTag("Character"))
            return;
        
        GameObject character = GameObject.FindWithTag("Character");
        if (character == null)
            return;

        // Dont Die when winding time back
        if (isRewindingCharacter.boolean)
            return;
        
        LifeManager lifeManager = character.GetComponent<LifeManager>();
        
        lifeManager.OnDeath();
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (!other.transform.CompareTag("Character"))
            return;
        
        GameObject character = GameObject.FindWithTag("Character");
        if (character == null)
            return;

        // Dont Die when winding time back
        if (isRewindingCharacter.boolean)
            return;
        
        LifeManager lifeManager = character.GetComponent<LifeManager>();
        
        lifeManager.OnDeath();
    }
}
