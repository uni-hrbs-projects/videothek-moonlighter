using System;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

public class SceneLoader : MonoBehaviour
{
 
 
    [FormerlySerializedAs("SceneName")] public string sceneName;
    private TrackTime _trackTime;

    private void Start()
    {
        _trackTime = FindObjectOfType<TrackTime>();
    }

    //void OnTriggerEnter2D(Collider2D other)
    //{
    //    print(other.gameObject.tag);
    //    if (other.gameObject.CompareTag("Character"))
    //    {
    //        print("test");
    //        SceneManager.LoadScene(sceneName); // loads scene When player enter the trigger collider
    //    }
    //}


    //void OnTriggerStay2D(Collider2D other)
    //{
    //    if (other.gameObject.CompareTag("Character"))
    //    {
    //        SceneManager.LoadScene(sceneName);
    //    }
    //}
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Character")
        {
            StartCoroutine(MoveOnToNextLevel());
        }
    }

    IEnumerator MoveOnToNextLevel()
    {
        _trackTime.Log("Player beat the Level after " + (Time.time - GameMaster.instance.startTime));
        CanvasFader.instance.FadeCanvas(0.25f, 1f);
        yield return new WaitForSecondsRealtime(0.75f);
        GameMaster.instance.ResetCheckpoint();
        SceneManager.LoadScene(sceneName);
    }
}