using TMPro;
using UnityEngine;

public class GameVersion : MonoBehaviour
{
    [SerializeField] private TMP_Dropdown _dropdown;
    [SerializeField] private IntergerSo gameVersion;
    
    public void DropdownChanged(int change)
    {
        gameVersion.integer = _dropdown.value;
        Debug.Log("Changed to: " + gameVersion.integer);
    }
}
