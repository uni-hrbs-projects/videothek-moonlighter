using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Integer", order = 2)]
public class IntergerSo : ScriptableObject
{
    public int integer;
}
