using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Bool", order = 2)]
public class BoolSo : ScriptableObject
{
    public bool boolean;
}
