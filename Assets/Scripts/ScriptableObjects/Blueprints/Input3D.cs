using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Input3D", order = 1)]
public class Input3D : ScriptableObject
{
    public Vector2 movement;
    public bool dash;
    public bool interact;
    public bool cancel;
    public bool next;

    public void ResetInput()
    {
        dash = interact = cancel = next = false;
        movement = Vector2.zero;
    }
}