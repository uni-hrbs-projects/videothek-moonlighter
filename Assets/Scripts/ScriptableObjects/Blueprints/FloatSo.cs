using UnityEngine;

[CreateAssetMenu(fileName = "Float", menuName = "ScriptableObjects/Float", order = 3)]
public class FloatSo : ScriptableObject
{
    public float FloatValue;
}
