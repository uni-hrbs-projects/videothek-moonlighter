using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Input2D", order = 1)]
public class Input2D : ScriptableObject
{
    public bool jump;
    public float jumpPressedAt;
    public bool jumpHeld;
    public bool crouch;
    public float horiontalInput;
    public bool dash;
    public bool activate;
    public bool rewindMePressed;
    public bool rewindMeReleased;
    public bool rewindWorldPressed;
    public bool rewindWorldReleased;
    public bool forwardWorldPressed;
    public bool forwardWorldReleased;
    public bool remote_throw;
    public bool controllerUsed = false;
    public bool directionRight;
    public bool directionLeft;

    public void ResetInput()
    {
        jump = crouch = dash = activate = rewindMePressed = rewindMeReleased = rewindWorldPressed = rewindWorldReleased = 
            forwardWorldPressed = forwardWorldReleased = remote_throw = directionRight = directionLeft = false;
        jumpPressedAt = horiontalInput = 0;
    }
}
