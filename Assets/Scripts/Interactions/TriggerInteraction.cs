using System.Collections;
using UnityEngine;

/// <summary>
///  Class that Triggers an Action when running through a Collider, can trigger multiple times
/// </summary>
public class TriggerInteraction : InteractionBaseClass
{
    [SerializeField] private float triggerActionCooldown = 0.1f;
    /// <summary>
    /// Set to 0 if no maximum
    /// </summary>
    [SerializeField] private int triggerMaximum = 0;
    
    private bool isOnCooldown;
    private int triggerCounter;
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.transform.CompareTag("Character"))
            return;

        if (triggerCounter >= triggerMaximum && triggerMaximum != 0)
            return;

        interactionAction?.Invoke();
        triggerCounter += 1;
        StartCoroutine(AwaitCooldown());
    }

    private IEnumerator AwaitCooldown()
    {
        isOnCooldown = true;
        yield return new WaitForSecondsRealtime(triggerActionCooldown);
        isOnCooldown = false;
    }
}
