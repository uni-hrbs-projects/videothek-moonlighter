using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Class that triggers an Action once when activating a Buttons
/// </summary>
public class ButtonInteraction : InteractionBaseClass
{
    // Action linked to a function that visually animates the button so the player knows he activated it
    [SerializeField] private UnityEvent buttonActiveAnimation;
    [SerializeField] private UnityEvent buttonDeactiveAnimation;
    [SerializeField] private UnityEvent UndoButtonActivationLogic;
    
    [HideInInspector] public bool isTriggered;
    public static List<Vector2> keepTriggered = new List<Vector2>();
    private GameMaster gameMaster;
    

    private void Start()
    {
        gameMaster = GameObject.FindGameObjectWithTag("GameMaster").GetComponent<GameMaster>();
        gameMaster.CheckpointSet += (sender, args) =>
        {
            safeState();
        };
        if(keepTriggered.Contains(transform.position))
            isTriggered = true;
        if(isTriggered)
            ChangeButtonState();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        // Not checking for Character layer as he could trigger the button by pushing stuff on top of it
        if (isTriggered || TimeBodyController.allRewind)
            return;
        
        isTriggered = true;
        ChangeButtonState();
    }

    public void safeState()
    {
        Vector2 position = this.transform.position;
        if(isTriggered && !keepTriggered.Contains(position))
            keepTriggered.Add(position);
    }
    
    public void ChangeButtonState()
    {
        if (isTriggered)
        {
            interactionAction?.Invoke();
            buttonActiveAnimation?.Invoke();
        }
        else
        {
            UndoButtonActivationLogic?.Invoke();
            buttonDeactiveAnimation?.Invoke();
        }
    }
}
