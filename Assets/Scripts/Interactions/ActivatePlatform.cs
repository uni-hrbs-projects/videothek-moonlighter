using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Class that triggers an Action when pressing the Interaction-Input-Button at a certain location
/// </summary>
public class ActivatePlatform : InteractionBaseClass
{
    [SerializeField] private Input2D _input2D;

    public UnityEvent exitAction;

    void OnTriggerStay2D(Collider2D other)
    {
        if (!other.transform.CompareTag("Character") )
            return;
        
        if (!_input2D.activate) 
            return;
        
        interactionAction?.Invoke();
        
        _input2D.activate = false;
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == ("Character") && exitAction.GetPersistentEventCount()!=0)
        {
            exitAction?.Invoke();
        }
    }
}
