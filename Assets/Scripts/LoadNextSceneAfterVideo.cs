using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Video;

public class LoadNextSceneAfterVideo : MonoBehaviour
{
    [SerializeField] private Image fill;
    [SerializeField] private float activationTime = 0.5f;
    [SerializeField] private Input3D input3D;
    private VideoPlayer _videoPlayer;
    private bool noInput;
    private float currentTime;

    private void Awake()
    {
        SoundManager soundManager = FindObjectOfType<SoundManager>();
        if(soundManager != null)
            soundManager.StopBGM();
        
        _videoPlayer = GetComponent<VideoPlayer>();
        _videoPlayer.loopPointReached += NextScene;
    }
    
    private void Update()
    {
        if (noInput)
            return;
        
        if (input3D.next)
            currentTime += Time.deltaTime;
        else
            currentTime -= Time.deltaTime;
        
        currentTime = Mathf.Clamp(currentTime, 0, activationTime);
        float percent = Mathf.Clamp01(currentTime / activationTime);
        
        fill.fillAmount = percent;
        if(percent == 1)
            NextScene(_videoPlayer);
    }
    
    private void NextScene(VideoPlayer vp)
    {
        Debug.Log("Loading Next Scene");
        noInput = true;
        _videoPlayer.Pause();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    private void OnDestroy()
    {
        _videoPlayer.loopPointReached -= NextScene;
    }
}
