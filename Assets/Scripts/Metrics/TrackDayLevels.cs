using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Metrics;
using UnityEngine.SceneManagement;

public class TrackDayLevels : MonoBehaviour
{
    public static TrackDayLevels instance;
    public LogWriter logWriter2D;
    private int count = 0;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
            logWriter2D = new LogWriter("Start Tracking Count of Day Levels...", "TrackDayLevels");
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    public void Log()
    {
        count++;
        logWriter2D.LogWrite(count + " times started the Day Level");
    }
    
}