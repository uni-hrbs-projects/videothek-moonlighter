using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Metrics;
using UnityEngine.SceneManagement;

public class TrackTime : MonoBehaviour
{
    public static TrackTime instance;
    public LogWriter logWriter2D;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
            logWriter2D = new LogWriter("Start Tracking Time...", "TrackTime");
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    public void Log(string Message)
    {
        logWriter2D.LogWrite(Message);
    }
    
}