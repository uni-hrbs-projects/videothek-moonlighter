using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Metrics;
using UnityEngine.SceneManagement;

public class Track3DButtons : MonoBehaviour
{
    public static Track3DButtons instance;
    public LogWriter logWriter2D;
    private Dictionary<string, int> _count = new Dictionary<string, int>();
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
            logWriter2D = new LogWriter("Start Tracking Buttons for 3D Scene...", "Track3DButtons");
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    public void LogButtonPress(string ButtonKey)
    {
        if (_count.ContainsKey(ButtonKey))
        {
            _count[ButtonKey] = _count[ButtonKey]+1;
        }
        else
        {
            _count.Add(ButtonKey, 1);
        }
        logWriter2D.LogWrite(ButtonKey + " " + _count[ButtonKey] + " times");
    }
    
}