using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Metrics;
using UnityEngine.SceneManagement;

public class TrackDeathCheckpoints : MonoBehaviour
{
    public static TrackDeathCheckpoints instance;
    public LogWriter logWriter2D;
    private Dictionary<string, int> _count = new Dictionary<string, int>();
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
            logWriter2D = new LogWriter("Start Tracking Deaths and Checkpoints...", "TrackDeathCheckpoints");
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    public void Log(string Key, string Message)
    {
        if (_count.ContainsKey(Key))
        {
            _count[Key] = _count[Key]+1;
        }
        else
        {
            _count.Add(Key, 1);
        }
        logWriter2D.LogWrite(Message + " " + _count[Key] + " times");
    }
    
}