using System;
using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem.Interactions;

namespace Metrics
{
    public class LogWriter
    {
        private string m_exePath = string.Empty;
        private string logName;
        public LogWriter(string logMessage, string logName)
        {
            this.logName = logName;
            LogWrite(logMessage);
        }
        public void LogWrite(string logMessage)
        {
            m_exePath = Application.dataPath + "\\StreamingAssets";
            try
            {
                using (StreamWriter w = File.AppendText(m_exePath + "\\" + logName + "_log.txt"))
                {
                    Log(logMessage, w);
                }
            }
            catch (Exception ex)
            {
            }
        }

        public void Log(string logMessage, TextWriter txtWriter)
        {
            try
            {
                txtWriter.Write("\r\nLog Entry : ");
                txtWriter.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                    DateTime.Now.ToLongDateString());
                txtWriter.WriteLine("  :");
                txtWriter.WriteLine("  :{0}", logMessage);
                txtWriter.WriteLine("-------------------------------");
            }
            catch (Exception ex)
            {
            }
        }
    }
}