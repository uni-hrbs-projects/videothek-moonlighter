using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SFX_Info: MonoBehaviour
{
    [Serializable]
    public struct SFX
    {
        public AudioClip sfx_Clip;
        public string action;
    }

    public List<SFX> sfx_List;

    SoundManager soundManager;

    private void Start()
    {

        soundManager = GameObject.FindObjectOfType<SoundManager>();
        if (soundManager == null)
        {
            Debug.Log("Sound Manager not Found");
        }
    }

    public void PlaySFX(string action)
    {
        if(soundManager != null)
            soundManager.PlaySFX(sfx_List.Find(a=>a.action==action).sfx_Clip);
    }

}
