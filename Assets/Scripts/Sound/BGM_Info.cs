using UnityEngine;

public class BGM_Info : MonoBehaviour
{
    public AudioClip bgm_Clip;
    SoundManager soundManager;

    private void Start()
    {
        soundManager = FindObjectOfType<SoundManager>();
        if(soundManager != null)
            soundManager.PlayBGM(bgm_Clip);
    }
        
}
