using System.Collections;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    private static SoundManager instance;

    public SoundManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<SoundManager>();
                if (instance == null)
                {
                    instance = new GameObject("SoundManager", typeof(SoundManager)).GetComponent<SoundManager>();
                }
            }
            return instance;
        }
        set
        {
            instance = value;
        }
    }

    AudioSource bgm;
    AudioSource sfx;

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);

        bgm = gameObject.AddComponent<AudioSource>();
        sfx = gameObject.AddComponent<AudioSource>();
        bgm.loop = true;
        bgm.volume = 0.1f;
    }

    public void PlayBGM(AudioClip bgmClip)
    {
        bgm.clip = bgmClip;
        bgm.Play();
        bgm.loop = true;
    }

    public void StopBGM()
    {
        bgm.Stop();
    }
    
    public void RewindBGM(bool rewind)
    {
        if (rewind)
        {
            bgm.pitch = -1;
            bgm.loop = true;

            StartCoroutine(StopLoop());
        }
        else
        {
            bgm.pitch = 1;
        }

    }
    public void FastforwardBGM(bool fastforward)
    {
        if(fastforward)
        {
            bgm.pitch = 2;
            StartCoroutine(StopForward());
        }
    }

    public void FastForwardWorldBGM(int pitch)
    {
        bgm.pitch = pitch;
    }
    
    private IEnumerator StopForward()
    {
        yield return new WaitForSecondsRealtime(1);
        bgm.pitch = 1;
    }
    
    private IEnumerator StopLoop()
    {
        yield return null;
        bgm.loop = false;
    }


    public void PlaySFX(AudioClip sfxClip)
    {
        sfx.PlayOneShot(sfxClip);
    }
}