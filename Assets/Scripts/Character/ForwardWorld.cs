using UnityEngine;
using UnityEngine.Events;

public class ForwardWorld : MonoBehaviour
{
    [SerializeField] private float forwardMultiplier;
    [SerializeField] private float batteryConsumption;
    [SerializeField] private Input2D playerInput;

    [SerializeField] private UnityEvent OnEffectStart;
    [SerializeField] private UnityEvent OnEffectFinish;
    [SerializeField] private SoundManager soundManager;
    private Track2DButtons _trackButtons;

    
    private bool isForwarding;
    private Rigidbody2D charRb;

    private void Start()
    {
        soundManager = FindObjectOfType<SoundManager>();
        charRb = GetComponent<Rigidbody2D>();
        _trackButtons = FindObjectOfType<Track2DButtons>();
    }

    private void SpeedUpTime()
    {
        if (BatteryManager.instance.GetBatteryCharge() < batteryConsumption)
            return;
        if(_trackButtons!=null)
            _trackButtons.LogButtonPress("World Forward");

        Debug.Log("Speeding up time");
        Time.timeScale = forwardMultiplier;
        
        Vector2 rbVelocity = charRb.velocity;
        if (rbVelocity.y != 0)
        {
            // Neccessary, so the applied force from jumping in regular time is adjusted to the new timescale
            Debug.Log("Adjusting Velocity - Timescale " + Time.timeScale);
            rbVelocity.y /= Time.timeScale;
        }        
        charRb.velocity = rbVelocity;
        
        OnEffectStart?.Invoke();
        if(soundManager != null)
            soundManager.FastForwardWorldBGM(2);
        
        isForwarding = true;
    }

    private void RegularTime()
    {
        if (!isForwarding)
            return;

        Debug.Log("Back to regular time");
        OnEffectFinish?.Invoke();
        if(soundManager != null)
            soundManager.FastForwardWorldBGM(1);
        
        Time.timeScale = 1;
        isForwarding = false;
    }

    private void Update()
    {
        if (playerInput.forwardWorldPressed)
        {
            SpeedUpTime();
            playerInput.forwardWorldPressed = false;
        }

        if (playerInput.forwardWorldReleased)
        {
            RegularTime();
            playerInput.forwardWorldReleased = false;
        }
        
        if (!isForwarding)
            return;

        bool batteryNotEmpty = BatteryManager.instance.Use_Battery(batteryConsumption * Time.deltaTime);
        if(!batteryNotEmpty)
            RegularTime();
    }

    private void OnDestroy()
    {
        if(Time.timeScale == forwardMultiplier)
            RegularTime(); 
    }
}
