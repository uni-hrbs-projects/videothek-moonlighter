using UnityEngine;
using UnityEngine.UI;

public class BatteryManager : MonoBehaviour
{
    public static BatteryManager instance;
    
    [SerializeField] private float fullAmount;
    [SerializeField] private FloatSo batteryAmount;
    private Slider battery_Slider;
    public Image gauge;

    private void Awake()
    {
        if(instance != null)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;

        battery_Slider = GetComponent<Slider>();
        Adjust_Battery();
    }

    public void Reset_Battery()
    {
        batteryAmount.FloatValue = fullAmount;
        Adjust_Battery();
    }
    public void Charge_Battery(float amount)
    {
        if (batteryAmount.FloatValue + amount > fullAmount)
            batteryAmount.FloatValue = fullAmount;
        else
            batteryAmount.FloatValue += amount;

        if (amount < 0)
            amount = 0;
        
        Adjust_Battery();
    }

    private void Adjust_Battery()
    {
        Set_BatteryGauge(batteryAmount.FloatValue);
        Color_Battery();
    }
    

    /// Consumes Battery, returns false if battery is empty
    public bool Use_Battery(float amount)
    {
        if (batteryAmount.FloatValue - amount < 0)
            return false;
        else
        {
            batteryAmount.FloatValue -= amount;
            Adjust_Battery();
            return true;
        }

    }

    public float GetBatteryCharge()
    {
        return batteryAmount.FloatValue;
    }

    public void SetBatteryCharge(float batteryValue)
    {
        batteryAmount.FloatValue = batteryValue;
        Adjust_Battery();
    }

    void Color_Battery()
    {
        if (batteryAmount.FloatValue >= 50)
            gauge.color = Color.green;
        else if (batteryAmount.FloatValue >= 21 && batteryAmount.FloatValue < 50)
            gauge.color = Color.yellow;
        else
            gauge.color = Color.red;
    }

    void Set_BatteryGauge(float batteryAmount)
    {
        battery_Slider.value = batteryAmount;
    }

    private void OnDestroy()
    {
        instance = null;
    }
}
