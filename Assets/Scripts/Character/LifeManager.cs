using System.Collections;
using Kino;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LifeManager : MonoBehaviour
{
    [HideInInspector] public bool isDying;
    [SerializeField] private BoolSo newGameFlag;
    [SerializeField] private Animator _animator;
    [SerializeField] private ParticleSystem deathPlosion;
    [SerializeField] private GameObject deathUi;
    private AnalogGlitch _analogGlitch;
    private GameMaster gameMaster;
    private BatteryManager battery;
    private TrackDeathCheckpoints _trackDeathCheckpoints;

    private void Start()
    {
        gameMaster = GameObject.FindGameObjectWithTag("GameMaster").GetComponent<GameMaster>();
        battery = GameObject.FindGameObjectWithTag("Battery").GetComponent<BatteryManager>();
        if (gameMaster.lastCheckPointPosition != Vector2.zero)
        {
            transform.position = gameMaster.lastCheckPointPosition;
            battery.SetBatteryCharge(gameMaster.batteryValue);
        }

        _analogGlitch = GetComponentInChildren<AnalogGlitch>();
        _trackDeathCheckpoints = FindObjectOfType<TrackDeathCheckpoints>();
    }

    private Coroutine regularDeath;

    public void OnDeath()
    {
        if (isDying)
            return;
        
        if(ScoreBasedPlaytime.instance != null)
            ScoreBasedPlaytime.instance.OnDeathReduction();
        
        // Removed previous Death Logic, as player can now only die once anyways
        if(_trackDeathCheckpoints!=null)
            _trackDeathCheckpoints.Log("Death", "Died");
        isDying = true;
        CharacterController2D.stopMovement = true;
        _animator.SetTrigger("GotHit");

        StartCoroutine(Death());
    }

    private IEnumerator Death()
    {
        gameMaster.checkCoinMultiplier();
        gameMaster.unsubscribeListeners();
        newGameFlag.boolean = false;
        yield return new WaitForSecondsRealtime(0.5f);
        LeanTween.value(gameObject, TweenGlitch, 0, 1, 2.5f).setEaseInOutQuad();
        deathUi.SetActive(true);
        yield return new WaitForSecondsRealtime(0.5f);
        GetComponentInChildren<SpriteRenderer>().enabled = false;
        yield return new WaitForSecondsRealtime(0.25f);
        deathPlosion.Play();
        yield return new WaitForSecondsRealtime(1f);
        CanvasFader.instance.FadeCanvas(0.25f, 1f);
        yield return new WaitForSecondsRealtime(0.75f);
        CharacterController2D.stopMovement = false;
        transform.position = gameMaster.lastCheckPointPosition;
        if(_trackDeathCheckpoints!=null)
            _trackDeathCheckpoints.Log(gameMaster.checkpointNumber.ToString(), gameMaster.lastCheckPointPosition.ToString() + " Checkpoint respawn");
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void TweenGlitch(float val)
    {
        _analogGlitch.colorDrift = val;
        _analogGlitch.horizontalShake = val;
        _analogGlitch.verticalJump = val;
        _analogGlitch.scanLineJitter = val;
    }
}
