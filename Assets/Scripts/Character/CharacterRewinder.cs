using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CharacterRewinder : MonoBehaviour
{
    public class CharacterTracker
    {
        public struct CharacterPointInTime
        {
            public float rotation;
            public Vector3 position;

            public CharacterPointInTime(float rotation, Vector3 position)
            {
                this.rotation = rotation;
                this.position = position;
            }
        }

        public List<CharacterPointInTime> CharacterPointInTimes = new List<CharacterPointInTime>();
    }


    [SerializeField] private Input2D playerInput;
     private CharacterTracker _characterTracker;
    [SerializeField] private BoolSo isRewinding;
    [SerializeField] private float maxTrackedTime = 5f;
    [SerializeField] private BoolSo disableMovement;
    [SerializeField] private UnityEvent OnEffectStart;
    [SerializeField] private UnityEvent OnEffectFinish;
    private Track2DButtons _trackButtons;
    
    [Header ("Battery")]
    [SerializeField] private byte batteryUsage;

    [Header("SoundEffect")]
    [SerializeField] private SoundManager soundManager;


    private bool isTracking = true;
    private Rigidbody2D characterRigidbody;
    private LifeManager _lifeManager;

    private void Awake()
    {
        _characterTracker = new CharacterTracker();
        characterRigidbody = GetComponent<Rigidbody2D>();
        _lifeManager = GetComponentInChildren<LifeManager>();
        soundManager = GameObject.FindObjectOfType<SoundManager>();   
    }

    private void Start()
    {
        _trackButtons = FindObjectOfType<Track2DButtons>();
    }

    private void StartRewind()
    {
        if (BatteryManager.instance.GetBatteryCharge() < batteryUsage)
            return;
        if(_trackButtons!=null)
            _trackButtons.LogButtonPress("Character Rewind");
        OnEffectStart?.Invoke();
        
        Debug.Log("Starting Rewind");
        if(soundManager != null)
            soundManager.RewindBGM(true);           //start rewinding sound
        disableMovement.boolean = true;
        characterRigidbody.velocity = Vector2.zero;
        characterRigidbody.isKinematic = true;
        isRewinding.boolean = true;
        CharacterController2D.stopMovement = true;
    }

    private void StopRewind()
    {
        if (!isRewinding.boolean) // Already Stopped
            return;
        
        OnEffectFinish?.Invoke();
        
        Debug.Log("Stopping Rewind");
        if(soundManager != null)
            soundManager.RewindBGM(false);           //stop rewinding sound
        characterRigidbody.velocity = Vector2.zero;
        characterRigidbody.isKinematic = false;
        isRewinding.boolean = false;
        disableMovement.boolean = false;
        CharacterController2D.stopMovement = false;
    }

    private void Update()
    {
        if (playerInput.rewindMePressed)
        {
            StartRewind();
            playerInput.rewindMePressed = false;
        }
        
        if (playerInput.rewindMeReleased)
        {
            StopRewind();
            playerInput.rewindMeReleased = false;
        }
    }

    private void FixedUpdate()
    {
        if (isRewinding.boolean)
        {
            if (_characterTracker.CharacterPointInTimes.Count <= 0)
            {
                StopRewind();
                return;
            }
            
            bool batteryNotEmpty = BatteryManager.instance.Use_Battery(batteryUsage * Time.fixedDeltaTime);

            if (!batteryNotEmpty)
            {
                StopRewind();
                return;
            }

            characterRigidbody.position = _characterTracker.CharacterPointInTimes[0].position;
            characterRigidbody.rotation = _characterTracker.CharacterPointInTimes[0].rotation;
            _characterTracker.CharacterPointInTimes.RemoveAt(0);
            return;
        }
        
        if (isTracking)
        {
            _characterTracker.CharacterPointInTimes.Insert(0, 
                new CharacterTracker.CharacterPointInTime(characterRigidbody.rotation, characterRigidbody.position));

            if (Time.fixedDeltaTime * _characterTracker.CharacterPointInTimes.Count >= maxTrackedTime)
                _characterTracker.CharacterPointInTimes.RemoveAt(_characterTracker.CharacterPointInTimes.Count - 1);
        }
    }
}
