using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;


/// <summary>
/// Modified Version from Brackeys Youtube Video
/// https://github.com/Brackeys/2D-Character-Controller
/// </summary>
public class CharacterController2D : MonoBehaviour
{
	public static bool stopMovement = false;
	
	[SerializeField] private Input2D _input2D;
	[SerializeField] private float m_gravity;
	[SerializeField] private float m_JumpForce = 400f;							// Amount of force added when the player jumps.
	[SerializeField] private float fallMultiplier = 2.5f;
	[SerializeField] private float lowJumpMultiplier = 2f;
	[SerializeField] private float jumpBufferTime = 0.1f;						// Time window in which another jump can be buffered before landing
	[SerializeField] private float walkOffForgiveness = 0.2f;					// Defines how long after running off a cliff the player can still jump	
	[SerializeField] private float moveSpeed = 10f;				// Defines Movementspeed
	[Range(0, 1)] [SerializeField] private float m_CrouchSpeed = .36f;			// Amount of maxSpeed applied to crouching movement. 1 = 100%
	[Range(0, .3f)] [SerializeField] private float m_MovementSmoothing = .05f;	// How much to smooth out the movement
	[SerializeField] private bool m_AirControl = false;							// Whether or not a player can steer while jumping;
	[SerializeField] private LayerMask m_WhatIsGround;							// A mask determining what is ground to the character
	[SerializeField] private Transform m_GroundCheck;							// A position marking where to check if the player is grounded.
	[SerializeField] private Transform m_CeilingCheck;							// A position marking where to check for ceilings
	[SerializeField] private Collider2D m_CrouchDisableCollider;				// A collider that will be disabled when crouching
	[SerializeField] private Animator _animator;
	[SerializeField] private ParticleEmissionOff runParticle;
	[SerializeField] private SFX_Info sfx_Info;                                 // Play sound effect

	[Header("Wall Jump")]
	[Space] 
	
	[SerializeField] private Transform forwardCheck;
	[SerializeField] private Transform backwardCheck;
	[SerializeField] private float slideSpeed;
	[SerializeField] private float wallJumpForce;
	[SerializeField] private float wallCheckRadius;
	[SerializeField] private float controlLossDuration;
	[SerializeField] private float controlLossStrength;
	private bool justWallJumped;
	private float timeGoneBy;
	private float isAtWall = 0;
	private bool wasJustWalled;

	const float k_GroundedRadius = .2f; // Radius of the overlap circle to determine if grounded
	private bool m_Grounded = true;            // Whether or not the player is grounded.
	const float k_CeilingRadius = .2f; // Radius of the overlap circle to determine if the player can stand up
	private Rigidbody2D m_Rigidbody2D;
	public bool m_FacingRight = true;  // For determining which way the player is currently facing.
	private Vector3 m_Velocity = Vector3.zero;
	private bool justJumped = false;
	private bool wasJustGrounded = false;

	[Header("Events")]
	[Space]

	public UnityEvent OnLandEvent;

	[System.Serializable]
	public class BoolEvent : UnityEvent<bool> { }

	public BoolEvent OnCrouchEvent;
	private bool m_wasCrouching = false;
	private Track2DButtons _trackButtons;

	private void Start()
	{
		_trackButtons = FindObjectOfType<Track2DButtons>();
	}

	private void Awake()
	{
		m_Rigidbody2D = GetComponent<Rigidbody2D>();

		if (OnLandEvent == null)
			OnLandEvent = new UnityEvent();

		if (OnCrouchEvent == null)
			OnCrouchEvent = new BoolEvent();

		stopMovement = false;
	}

	private void FixedUpdate()
	{
		// Bool Flag to completly stop Character Movement as setting Time Scale to 0 won't do anymore.
		if (stopMovement)
		{
			m_Rigidbody2D.velocity = Vector2.zero;
			return;
		}	
		
		bool wasGrounded = m_Grounded;
		m_Grounded = false;

		// The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
		// This can be done using layers instead but Sample Assets will not overwrite your project settings.
		Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
		bool onPlatform = false;
		for (int i = 0; i < colliders.Length; i++)
		{
			if (colliders[i].gameObject != gameObject)
			{
				m_Grounded = wasJustGrounded = true;
				if (!wasGrounded)
				{
					OnLandEvent.Invoke();
					runParticle.ParticleOn();
					_animator.SetTrigger("Landed");
				}			
			}
			
			if (colliders[i].gameObject.CompareTag("Platform")){
				transform.parent = colliders[i].gameObject.transform;
				onPlatform = true;
			}
		}

		bool was_walled = isAtWall != 0;
		
		// Check if hangig at Wall
		if (Physics2D.OverlapCircle(forwardCheck.position, wallCheckRadius, m_WhatIsGround))
			isAtWall = 1;
		else if (Physics2D.OverlapCircle(backwardCheck.position, wallCheckRadius, m_WhatIsGround))
			isAtWall = -1;
		else
			isAtWall = 0;

		if (isAtWall == 0 && was_walled)
		{
			StartCoroutine(WallWalkOffForgiveness());
		}
		
		
		if(!onPlatform)
		{
			transform.parent = null;
		}
		
		// Is not grounded but just was
		if (!m_Grounded && wasGrounded)
		{
			StartCoroutine(WalkOffForgiveness());
			runParticle.ParticleOff();
		}
		
		// Allow buffering a jump for a split second
		bool bufferedJump = false;
		if(_input2D.jump)
		{
			if (Time.time - _input2D.jumpPressedAt <= jumpBufferTime)
				bufferedJump = true;
			else
				_input2D.jump = false;
		}
		
		_animator.SetFloat("Speed", Mathf.Abs(_input2D.horiontalInput));
		Move(_input2D.horiontalInput, _input2D.crouch, bufferedJump);

		
		// https://www.youtube.com/watch?v=7KiK0Aqtmzc
		// Better jumping with gravity instead of added force as shown in Video and marginalising TimeScale differences
		if (m_Rigidbody2D.velocity.y < 0)
			m_Rigidbody2D.velocity += Vector2.up * -m_gravity * Time.fixedUnscaledDeltaTime * 1 / Time.timeScale * (fallMultiplier - 1);
		else if(m_Rigidbody2D.velocity.y > 0 && !_input2D.jumpHeld)
			m_Rigidbody2D.velocity += Vector2.up * -m_gravity * Time.fixedUnscaledDeltaTime * 1 / Time.timeScale * (lowJumpMultiplier - 1);

		// Apply Gravity
		// To cite the great Half-Life Devs: "I don't know why this works, but it works."
		// (Referring to the 1/Time.timescale to get the right jump in sped up time).
		m_Rigidbody2D.velocity += Vector2.down * m_gravity * Time.fixedUnscaledDeltaTime * 1/Time.timeScale;
		
		
		
		// Wall Logic [Could encapsulated in another bool to lock/unlock wall-behaviour
		
		if(isAtWall != 0 && m_Rigidbody2D.velocity.y < 0) // Player should slide Down the Wall
		{
			// Flip Character to face the wall
			if(isAtWall == -1)
				Flip();
				
			// TODO: Wall-Slide needs a seperate Animation!
			m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x, -slideSpeed * Time.fixedUnscaledDeltaTime);
		}
		
		if (bufferedJump && !justJumped && !m_Grounded)
		{
			// Regular Wall Jump
			if (isAtWall != 0)
			{
				// (transform.localScale.x * -1) Determines the correct Direction to apply force towards
				float horizontalJumpforce = (transform.localScale.x * -1) * Time.fixedDeltaTime * wallJumpForce;

				// Flip Character if he was facing the wall
				if(isAtWall == 1)
					Flip();

				SharedJumpLogic();
				m_Rigidbody2D.velocity = new Vector2(horizontalJumpforce, m_JumpForce * Time.fixedUnscaledDeltaTime);
				StartCoroutine(WallJumpControlPenalty());
			}
			
			// Delayed Wall Jump
			if (wasJustWalled)
			{
				// In order to be in the "wasJustWalled"-State, the player must have faced away from the wall, so flip the jump force
				float horizontalJumpforce = transform.localScale.x * Time.fixedDeltaTime * wallJumpForce;

				// Flip Character if he was facing the wall
				if(isAtWall == 1)
					Flip();

				SharedJumpLogic();
				m_Rigidbody2D.velocity = new Vector2(horizontalJumpforce, m_JumpForce * Time.fixedUnscaledDeltaTime);
				StartCoroutine(WallJumpControlPenalty());
			}
		}
	}

	private void Move(float move, bool crouch, bool jump)
	{
		/*
		// If crouching, check to see if the character can stand up
		if (!crouch)
		{
			// If the character has a ceiling preventing them from standing up, keep them crouching
			if (Physics2D.OverlapCircle(m_CeilingCheck.position, k_CeilingRadius, m_WhatIsGround))
			{
				crouch = true;
			}
		}
		*/

		//only control the player if grounded or airControl is turned on
		if (m_Grounded || m_AirControl)
		{
			/*
			// If crouching
			if (m_Grounded) // Crouch Logic not in Air!
			{
				if (crouch)
				{
					if (!m_wasCrouching)
					{
						m_wasCrouching = true;
						OnCrouchEvent.Invoke(true);
					}

					// Reduce the speed by the crouchSpeed multiplier
					move *= m_CrouchSpeed;

					// Disable one of the colliders when crouching
					if (m_CrouchDisableCollider != null)
						m_CrouchDisableCollider.enabled = false;
				} else
				{
					// Enable the collider when not crouching
					if (m_CrouchDisableCollider != null)
						m_CrouchDisableCollider.enabled = true;

					if (m_wasCrouching)
					{
						m_wasCrouching = false;
						OnCrouchEvent.Invoke(false);
					}
				}
			}
			*/

			// Move the character by finding the target velocity
			Vector3 targetVelocity = new Vector2(move * moveSpeed * Time.fixedUnscaledDeltaTime, m_Rigidbody2D.velocity.y);
			
			// And then smoothing it out and applying it to the character
			if (justWallJumped) // Increase the smoothing after a walljump
				m_Rigidbody2D.velocity = Vector3.SmoothDamp(m_Rigidbody2D.velocity, targetVelocity, ref m_Velocity, controlLossStrength);
			else
				m_Rigidbody2D.velocity = Vector3.SmoothDamp(m_Rigidbody2D.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);
			
			

			if(isAtWall == 0) // dont flip character for movement while at wall
			{
				// If the input is moving the player right and the player is facing left...
				if (move > 0 && !m_FacingRight)
				{
					// ... flip the player.
					Flip();
				}
				// Otherwise if the input is moving the player left and the player is facing right...
				else if (move < 0 && m_FacingRight)
				{
					// ... flip the player.
					Flip();
				}
			}
		}

		// If the player should jump...
		if (jump && !justJumped)
		{
			if (wasJustGrounded)
			{
				if(_trackButtons!=null)
					_trackButtons.LogButtonPress("Jumped");
				SharedJumpLogic();

				// Add a vertical force to the player.
				m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x, m_JumpForce * Time.fixedUnscaledDeltaTime);
			}
		}
	}

	private void SharedJumpLogic()
	{
		//Play sound
		sfx_Info.PlaySFX("Jump");
		
		// Added visuals
		_animator.SetTrigger("Jump");
		runParticle.ParticleOff();
		
		// Reset y-Velocity before jumping
		Vector2 currentVel = m_Rigidbody2D.velocity;
		currentVel.y = 0;
		m_Rigidbody2D.velocity = currentVel;
		
		StartCoroutine(JumpCooldown());

		// Unflag Jump-Flags
		_input2D.jump = false;
		m_Grounded = false;
		wasJustGrounded = false;
		isAtWall = 0;
	}

	// This is to prevent a double jump through the buffering mechanic
	private IEnumerator JumpCooldown()
	{
		justJumped = true;
		yield return new WaitForSecondsRealtime(jumpBufferTime);
		justJumped = false;
	}

	// This allows jumping slightly after walking off an edge
	private IEnumerator WalkOffForgiveness()
	{
		wasJustGrounded = true;
		yield return new WaitForSecondsRealtime(walkOffForgiveness);
		wasJustGrounded = false;
	}
	
	// This allows jumping slightly after letting go off the Wall
	private IEnumerator WallWalkOffForgiveness()
	{
		wasJustWalled = true;
		yield return new WaitForSecondsRealtime(walkOffForgiveness);
		wasJustWalled = false;
	}
	
	// Prevent Player from instantly moving back to the wall
	private IEnumerator WallJumpControlPenalty()
	{
		justWallJumped = true;
		timeGoneBy = controlLossDuration;
		while (timeGoneBy > 0)
		{
			yield return null;
			timeGoneBy -= Time.deltaTime;
		}

		timeGoneBy = 0;
		justWallJumped = false;
	}

	private void Flip()
	{
		// Switch the way the player is labelled as facing.
		m_FacingRight = !m_FacingRight;

		// Multiply the player's x local scale by -1.
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	private void OnDrawGizmosSelected()
	{
		Gizmos.DrawSphere(m_GroundCheck.position, k_GroundedRadius);
		Gizmos.DrawSphere(forwardCheck.position, k_GroundedRadius);
		Gizmos.DrawSphere(backwardCheck.position, k_GroundedRadius);
	}

	private void OnDestroy()
	{
		stopMovement = false;
	} 
}
