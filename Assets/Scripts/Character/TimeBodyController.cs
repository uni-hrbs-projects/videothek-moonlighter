using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Tilemaps;

public class TimeBodyController : MonoBehaviour
{
	[SerializeField] private Input2D playerInput;
	[SerializeField] private UnityEvent OnEffectStart;
	[SerializeField] private UnityEvent OnEffectFinish;
	[SerializeField] private float batterCost = 20;
    public static bool allRewind;
    public static float recordTime = 10;
    private Track2DButtons _trackButtons;

    
    // Start is called before the first frame update
    void Awake()
    {
        allRewind = false;
    }

    private void Start()
    {
	    _trackButtons = FindObjectOfType<Track2DButtons>();
    }

    // Update is called once per frame
    void Update()
    {
	    if (BatteryManager.instance.GetBatteryCharge() < batterCost)
	    {
		    if(allRewind){
			    OnEffectFinish?.Invoke();
		    }
		    allRewind = false;
		    return;
	    }	
		if (playerInput.rewindWorldPressed)
		{
            if(!allRewind){
		        OnEffectStart?.Invoke();
		        if(_trackButtons!=null)
					_trackButtons.LogButtonPress("World Rewind");
            }
			allRewind = true;
			playerInput.rewindWorldPressed = false;
		}		
		
		if (playerInput.rewindWorldReleased)
		{
            if(allRewind){
		        OnEffectFinish?.Invoke();
            }
			allRewind = false;
			playerInput.rewindWorldReleased = false;
		}

		if (allRewind)
		{
			BatteryManager.instance.Use_Battery(batterCost * Time.deltaTime);
		}
        
    }
}
