using UnityEngine;
using UnityEngine.InputSystem;

public class CharacterInput2D : MonoBehaviour
{
    [SerializeField] private Input2D input;
    private bool _worldForward;
    private bool _worldBackward;
    private bool _meBackward;

    public void OnHorizontal(InputAction.CallbackContext value)
    {
        input.horiontalInput = value.ReadValue<float>();
        if (value.ReadValue<float>() > 0)
        {
            input.directionRight = true;
            input.directionLeft = false;
        }
        if (value.ReadValue<float>() < 0)
        {
            input.directionLeft = true;
            input.directionRight = false;
        }
    }

    public void OnJump(InputAction.CallbackContext value)
    {
        if (value.phase == InputActionPhase.Started)
        {
            input.jump = true;
            input.jumpPressedAt = Time.time;
        }

        input.jumpHeld = value.phase == InputActionPhase.Performed;
    }

    public void OnCrouch(InputAction.CallbackContext value)
    {
        input.crouch = value.performed;
    }

    public void OnInteraction(InputAction.CallbackContext value)
    {
        input.activate = value.performed;
    }

    public void OnR1(InputAction.CallbackContext value)
    {
        if (!_meBackward)
        {
            input.dash = value.performed;
        }
    }

    public void OnR2(InputAction.CallbackContext value)
    {
        if (!_worldBackward)
        {
            switch (value.phase)
            {
                case InputActionPhase.Started:
                    input.forwardWorldPressed = true;
                    _worldForward = true;
                    break;
                case InputActionPhase.Canceled:
                    input.forwardWorldReleased = true;
                    _worldForward = false;
                    break;
            }
        }
    }

    public void OnL1(InputAction.CallbackContext value)
    {
        switch (value.phase)
        {
            case InputActionPhase.Started:
                input.rewindMePressed = true;
                _meBackward = true;
                break;
            case InputActionPhase.Canceled:
                input.rewindMeReleased = true;
                _meBackward = false;
                break;
        }
    }

    public void OnL2(InputAction.CallbackContext value)
    {
        if (!_worldForward)
        {
            switch (value.phase)
            {
                case InputActionPhase.Started:
                    input.rewindWorldPressed = true;
                    _worldBackward = true;
                    break;
                case InputActionPhase.Canceled:
                    input.rewindWorldReleased = true;
                    _worldBackward = false;
                    break;
            }
        }
    }

    public void OnThrow(InputAction.CallbackContext value)
    {
        input.remote_throw = value.performed;
    }

    public void onControllerUsage(InputAction.CallbackContext value)
    {
        input.controllerUsed = true;
    }
    
     public void onMouseUsage(InputAction.CallbackContext value)
     {
         input.controllerUsed = false;
     }
}