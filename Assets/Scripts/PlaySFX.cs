using UnityEngine;

public class PlaySFX : MonoBehaviour
{
    [SerializeField] private AudioClip[] sfx;
    private SoundManager soundManager;

    private void Start()
    {
        soundManager = FindObjectOfType<SoundManager>();
    }


    public void PlaySfx(int toPlay)
    {
        if(soundManager != null)
            soundManager.PlaySFX(sfx[toPlay]);
    }
}
