using UnityEngine;
using UnityEngine.SceneManagement;

public class UpdateLevelToLoad : MonoBehaviour
{
    [SerializeField] private IntergerSo levelToLoad;

    private void Awake()
    {
        levelToLoad.integer = SceneManager.GetActiveScene().buildIndex;
    }
}
