using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

[RequireComponent(typeof(CanvasGroup))]
public class CanvasFader : MonoBehaviour
{
    public static CanvasFader instance;
    
    private CanvasGroup _canvasGroup;
    [SerializeField] private PlayerInput _playerInput;
    [SerializeField] private UnityEvent onFadeOut;
    [SerializeField] private float fadeOutTime = 2;

    private void Awake()
    {
        if (instance != null)
            Destroy(instance.gameObject);
        instance = this;
        _canvasGroup = GetComponent<CanvasGroup>();
        FadeCanvas(0.25f, 0);
    }

    public void FadeCanvas(float duration, float targetAlpha)
    {
        ForbidClicking();
        
        if(targetAlpha == 1)
            LeanTween.alphaCanvas(_canvasGroup, targetAlpha, duration).setIgnoreTimeScale(true).setOnComplete(AllowClicking);
        
        if(targetAlpha == 0)
            LeanTween.alphaCanvas(_canvasGroup, targetAlpha, duration).setIgnoreTimeScale(true).setOnComplete(AllowClicking);
    }

    public void FadeOut()
    {
        Time.timeScale = 0;
        ForbidClicking();
        LeanTween.alphaCanvas(_canvasGroup, 1, fadeOutTime).setIgnoreTimeScale(true).setOnComplete(() =>
        {
            AllowClicking();
            onFadeOut?.Invoke();
            Time.timeScale = 1;
        });
    }

    private void ForbidClicking()
    {
        _canvasGroup.interactable = true;
        _canvasGroup.blocksRaycasts = true;
        if(_playerInput != null)
            _playerInput.enabled = false;
    }

    private void AllowClicking()
    {
        _canvasGroup.interactable = false;
        _canvasGroup.blocksRaycasts = false;
        if(_playerInput != null)
            _playerInput.enabled = true;
    }
    
    private void OnDestroy()
    {
        instance = null;
    }
}
