using UnityEngine;

public abstract class AbstractSelectionAction : MonoBehaviour
{
    public abstract void SelectThisObject();

    public abstract void UnselectThisObject();
}
