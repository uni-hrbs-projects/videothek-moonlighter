using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

public class MouseHoverSelection : MonoBehaviour
{
    [SerializeField] private EventSystem eventSystem;

    public void OnMouseMove(InputAction.CallbackContext value)
    {
        // Get Moouse Pos here.
        Vector2 mouseInput = value.ReadValue<Vector2>();
        
        if (eventSystem.IsPointerOverGameObject())
        {    
            PointerEventData eventDataCurrentPosition = new PointerEventData(eventSystem)
            {
                position = mouseInput
            };

            var results = new List<RaycastResult>();
            eventSystem.RaycastAll(eventDataCurrentPosition, results);

            if (results.Count == 0)
                return;
            
            foreach (var result in results)
            {
                if(result.gameObject.CompareTag("UISelectable"))
                    eventSystem.SetSelectedGameObject(result.gameObject);
            }
        }
    }
}
