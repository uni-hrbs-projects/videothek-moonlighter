using TMPro;
using UnityEngine;

public class OnSelectionColorText : AbstractSelectionAction
{
    [SerializeField] private TextMeshProUGUI textElement;
    [SerializeField] private Color selectedColor;
    [SerializeField] private Color unselectedColor;
    
    public override void SelectThisObject()
    {
        textElement.color = selectedColor;
    }

    public override void UnselectThisObject()
    {
        textElement.color = unselectedColor;
    }
}
