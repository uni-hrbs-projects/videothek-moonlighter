using UnityEngine.EventSystems;
using UnityEngine;

public class UpdateSelected : MonoBehaviour
{
    [SerializeField] private EventSystem _eventSystem;
    private GameObject lastSelection;

    private void Awake()
    {
        UpdateSelection(_eventSystem.firstSelectedGameObject);
    }

    private void Update()
    {
        if(lastSelection != _eventSystem.currentSelectedGameObject)
            UpdateSelection(_eventSystem.currentSelectedGameObject);
    }

    private void UpdateSelection(GameObject newSelection)
    {
        if (newSelection == null)
            return;
        
        // Unselect the Previous object
        if (lastSelection != null)
        {
            AbstractSelectionAction unselection = lastSelection.GetComponent<AbstractSelectionAction>();
            if(unselection != null)
                unselection.UnselectThisObject();
        }
        
        // Select the New Object
        lastSelection = newSelection;
        AbstractSelectionAction selection = newSelection.GetComponent<AbstractSelectionAction>();

        if (selection == null)
            return;
        
        selection.SelectThisObject();
    }
}
