using Kino;
using UnityEngine;

public class GlitchEffectOnTimeWarp : MonoBehaviour
{
    [SerializeField] private AnalogGlitch _analogGlitch;

    private void OnEnable()
    {
        _analogGlitch.colorDrift = 0.04f;
        _analogGlitch.horizontalShake = 0.05f;
        _analogGlitch.verticalJump = 0.065f;
        _analogGlitch.scanLineJitter = 0.04f;
    }
    
    private void OnDisable()
    {
        _analogGlitch.colorDrift = 0;
        _analogGlitch.horizontalShake = 0;
        _analogGlitch.verticalJump = 0;
        _analogGlitch.scanLineJitter = 0;
    }
}
