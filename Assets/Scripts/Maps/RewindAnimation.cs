using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewindAnimation : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if (TimeBodyController.allRewind)
        {
            this.gameObject.GetComponent<Animator>().Play("waterfall_rewind");
        }
        else
        {
            this.gameObject.GetComponent<Animator>().Play("waterfall_anim");
        }
    }
}
