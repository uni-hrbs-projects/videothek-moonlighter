using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecursiveBackground : MonoBehaviour
{
    public Transform character;
    public Camera backgroundCamera;
     float startPos { get; set; }
    public BackgroundMap[] backMap=new BackgroundMap[2];

    float leftBoundary;
    float rightBoundary;

    public enum direction {Left,Right }
    public direction dir;

    byte backIndex;

    [Range(0,50)]
    public byte speed;

    private void Start()
    {
        startPos = character.position.x;
        Init_backMap();
        Init_Boundaries();
    }
    private void Init_backMap()
    {
        for(int i=0;i<2;i++)
        {
            backMap[i] = transform.GetChild(i).GetComponent<BackgroundMap>();
        }
    }
    float boundarySize = 1f;
    private void Init_Boundaries()
    {
       leftBoundary = backgroundCamera.transform.position.x - backMap[0].mapWidth* boundarySize;
       rightBoundary = backgroundCamera.transform.position.x + backMap[0].mapWidth* boundarySize;
    }

    bool Check_Boundary(BackgroundMap backMap)
    {
        if (backMap.transform.position.x <= leftBoundary )
        {
            dir = direction.Left;
            return true;
        }
        if (backMap.transform.position.x >= rightBoundary)
        {
            dir = direction.Right;
            return true;
        }
        else
            return false;

           
    }
    void Relocate_Background(BackgroundMap backMap)
    {
        Vector3 relocation;
        float relocateX;
        if (dir == direction.Left)
        {
            relocateX = this.backMap[Otehrindex(backIndex)].transform.position.x + backMap.mapWidth;
        }
        else
        {
            relocateX = this.backMap[Otehrindex(backIndex)].transform.position.x - backMap.mapWidth;
        }
        relocation = new Vector3(relocateX, backMap.transform.position.y, backMap.transform.position.z);
        backMap.transform.position = relocation;
    }
    float Calibrate_Frame(BackgroundMap backMap)
    {
        float gap=startPos - character.position.x;
        float percentage = gap/backMap.mapWidth;
        if(percentage>=1 || percentage<=-1)
        {
            percentage =percentage % backMap.mapWidth;
        }
        return percentage;
    }
    void Move_Background(BackgroundMap backMap)
    {


        backMap.transform.position = new Vector3(backMap.transform.position.x + Calibrate_Frame(backMap) * speed , backMap.transform.position.y, backMap.transform.position.z) ;

        if(backIndex==1)
        startPos = character.position.x;

    }
    void Go(BackgroundMap backMap)
    {
        if (Check_Boundary(backMap))
        {
            Relocate_Background(backMap);
        }

        Move_Background(backMap);
    }
    byte Otehrindex(byte i)
    {
        if(i==0) { return 1; } else { return 0; }
    }

    private void Update()
    {
        for(byte i=0;i<2;i++)
        {
            backIndex = i;
            Go(backMap[i]);
        }

    }

}
