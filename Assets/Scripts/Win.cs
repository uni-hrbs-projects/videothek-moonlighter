using UnityEngine;
using UnityEngine.Events;

public class Win : MonoBehaviour
{
    [SerializeField] private UnityEvent onHit;
    
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (!other.transform.CompareTag("Character"))
            return;
        
        onHit?.Invoke();
    }
}
