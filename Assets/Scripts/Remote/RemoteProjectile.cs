using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoteProjectile : MonoBehaviour
{
    public float speed;
    public float lifetime;
    private bool _lifetimeEnd = false;
    private Vector3 _startVector;
    private GameObject character;
    [SerializeField] private LayerMask m_WhatIsGround;							// A mask determining what is ground to the character
    [SerializeField] private Transform m_GroundCheck;							// A position marking where to check if the player is grounded.
    const float k_GroundedRadius = .002f;                                         // Radius of the overlap circle to determine if grounded
    [SerializeField] private float proximityDistance = 1;

    [SerializeField] private GameObject hitEffect;
    
    private void Start()
    {
        character = GameObject.FindWithTag("Character");
        _startVector = Vector3.up;
        Invoke(nameof(ReturnProjectile), lifetime);
    }

    // Update is called once per frame
    private void Update()
    {
        if (!_lifetimeEnd)
        {
            Collider2D[] colliders =
                Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].gameObject != gameObject)
                {
                    ReturnProjectile();
                    Instantiate(hitEffect, transform.position, transform.rotation);
                }
            }
        }

        if (_lifetimeEnd)
        {
            Vector3 newVector = character.transform.position - gameObject.transform.position;
            transform.Translate(newVector * (speed * Time.deltaTime));
            float distance = Mathf.Abs(Vector3.Distance(transform.position, character.transform.position));
            if (distance<proximityDistance)
            {
                character.GetComponentInChildren<RemoteThrow>().remoteIsThrown = false;
                Destroy(gameObject);
            }
        }
        else
        {
            transform.Translate(_startVector * (speed * Time.deltaTime));
        }
    }

    private void ReturnProjectile()
    {
        Vector3 eulerRotation = transform.rotation.eulerAngles;
        gameObject.transform.rotation = Quaternion.Euler(eulerRotation.x, eulerRotation.y, 0);
        _lifetimeEnd = true;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.gameObject.CompareTag("Character"))
        {
            ReturnProjectile();
            Instantiate(hitEffect, transform.position, transform.rotation);
        }
    }
}
