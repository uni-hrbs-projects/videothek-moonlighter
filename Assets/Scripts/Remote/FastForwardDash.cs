using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class FastForwardDash : MonoBehaviour
{
    [SerializeField] private Input2D _input2D;
    [SerializeField] private float batteryCost;
    [SerializeField] private UnityEvent OnDashEvent;
    [SerializeField] private UnityEvent OnDashEndEvent;
    
    private Rigidbody2D rb;
    public float dashSpeed;
    private float dashCooldown;
    public float startDashCooldown;
    private bool dashing;
    public GameObject character;
    private Track2DButtons _trackButtons;

    //sound
    [Header("SoundEffect")]
    [SerializeField] private SoundManager soundManager;

    // Start is called before the first frame update
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        dashCooldown = startDashCooldown;
        soundManager = FindObjectOfType<SoundManager>();
        _trackButtons = FindObjectOfType<Track2DButtons>();
    }

    // Update is called once per frame
    private void Update()
    {
        if(_input2D.dash & !dashing)
            Dash();
    }

    // TODO: Remove when functions not on Button anymore
    private void Dash()
    {
        if(_trackButtons!=null)
            _trackButtons.LogButtonPress("Dash");
        if (BatteryManager.instance.GetBatteryCharge() < batteryCost || dashing)
            return;

        OnDashEvent?.Invoke();
        
        BatteryManager.instance.Use_Battery(batteryCost);
        if(soundManager != null)
            soundManager.FastforwardBGM(true);   //BGM fastforward
        dashing = true;
        StartCoroutine(IncreaseVelocity());
        _input2D.dash = false;
        StartCoroutine(DashCooldown());
        StartCoroutine(DelayedDashEndEvent());
    }

    private IEnumerator DelayedDashEndEvent()
    {
        yield return new WaitForSecondsRealtime(0.33f);
        OnDashEndEvent?.Invoke();
    }

    private IEnumerator DashCooldown()
    {
        yield return new WaitForSeconds(dashCooldown);
        if(soundManager != null)
            soundManager.FastforwardBGM(false);  //BGM fastforward
        dashing = false;
    }

    private IEnumerator IncreaseVelocity()
    {
        yield return new WaitForFixedUpdate();
        Vector2 directionVector = character.GetComponent<CharacterController2D>().m_FacingRight ? Vector3.right : Vector3.left;
        if (_input2D.directionRight)
        {
            directionVector = Vector2.right;
        }

        if (_input2D.directionLeft)
        {
            directionVector = Vector2.left;
        }
        
        if (_input2D.jumpHeld)
        {
            directionVector = Vector2.up;
            if (_input2D.directionRight)
            {
                directionVector = (directionVector/2 + Vector2.right/2);
            }

            if (_input2D.directionLeft)
            {
                directionVector = (directionVector/2 + Vector2.left/2);
            }
        }
        rb.velocity = directionVector * dashSpeed;
    }
}
