using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class RemoteThrow : MonoBehaviour
{
    [SerializeField] private Input2D playerInput;
    [SerializeField] private float proximity = 0.1f;

    public float offset;
    public GameObject projectile;
    public Transform shotPoint;

    public bool remoteIsThrown;
    private float rotZ;
    private Vector2 directionLastFrame;
    private Track2DButtons _trackButtons;

    private void Start()
    {
        remoteIsThrown = false;
        rotZ = 0;
        _trackButtons = FindObjectOfType<Track2DButtons>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Gamepad.current != null && playerInput.controllerUsed)
        {
            Vector2 direction = Gamepad.current.rightStick.ReadValue();
            float distance = Vector2.Distance(Vector2.zero, direction);

            if (distance > proximity)
            {
                rotZ = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            }
            else
            {
                distance = Vector2.Distance(Vector2.zero, directionLastFrame);
                if (!remoteIsThrown && distance > proximity)
                    throwRemote();
            }

            directionLastFrame = direction;
        }
        if (Mouse.current != null && !playerInput.controllerUsed)
        {
            Vector3 difference = Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue()) -
                                 transform.position;
            rotZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        }
        
        transform.rotation = Quaternion.Euler(0f, 0f, rotZ + offset);


        if (!remoteIsThrown)
        {
            if (playerInput.remote_throw)
            {
                throwRemote();
            }
        }
    }

    private void throwRemote()
    {
        if(_trackButtons != null)
            _trackButtons.LogButtonPress("Remote Throw");
        remoteIsThrown = true;
        var position = shotPoint.position;
        Instantiate(projectile, position, transform.rotation);
    }
}
