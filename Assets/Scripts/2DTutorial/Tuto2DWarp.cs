using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Tuto2DWarp : MonoBehaviour
{
    public static Tuto2DWarp instance;
    private void Awake()
    {
        if (instance != null)
            Destroy(instance.gameObject);
        instance = this;
    }
    public enum Course { Nothing,Main, Tutorial, Movement, Ability,WR,CR,CF,WF, Quit }

    public Course currentCourse=Course.Main;

    [SerializeField]
    private SerializableDictionary<Course, Transform> warpzone;
    GameObject character;

    public Resetable resetManager;
    private void Start()
    {
        character = GameObject.FindGameObjectWithTag("Character");
    }

    public void Warp(Course course)
    {
        if (course == Course.Quit)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            return;
        }
        
        character.transform.position = warpzone[course].position;
        currentCourse = course;
        Unshackle(course);
        resetManager.ResetAll();
        character.GetComponentInChildren<BatteryManager>().Reset_Battery();
    }
     void Unshackle(Course course)
    {
        switch(course)
        {
            case Course.Main:
            case Course.Tutorial:
                character.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
                break; 
            default:
                character.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
                break;

        }
    }
}
