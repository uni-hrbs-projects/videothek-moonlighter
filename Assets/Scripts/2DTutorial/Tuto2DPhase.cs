using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Tuto2DPhase :MonoBehaviour
{
    public Tuto2DWarp.Course course;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag=="Projectile")
        {
            Destroy(collision);
            Tuto2DWarp.instance.Warp(course);
        }

    }


}
