using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tuto2DWallfloor : MonoBehaviour
{
    [Header("ParticlePlay")]
    public ParticlePlay pp;
    
    private void Start()
    {
        pp._particleSystem.startColor = Color.white;
    }
    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Projectile"))
        {
            collision.GetComponentInParent<DestoryObject>().DestroyMe();
            pp._particleSystem.transform.position = collision.transform.position;
            pp.PlayParticle();
        }
    }

}
