using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasFader2 : MonoBehaviour
{   
    public CanvasGroup canvasGroup;
    [Range(0, 5)]
    public float duration;
    bool vir;
    public static bool open;

    //target should be 0 or 1
    public void Fade(int target)
    {
        FadeCanvas(target);
        vir = true;
    }
     void FadeCanvas(int targetAlpha)
    {
        if (!vir)
        {
            LeanTween.alphaCanvas(canvasGroup, targetAlpha, duration).setIgnoreTimeScale(true);
        }
        else
        {
            targetAlpha = open ? 0 : 1;
            LeanTween.alphaCanvas(canvasGroup, targetAlpha, duration).setIgnoreTimeScale(true);
        }
        open = targetAlpha == 1 ? true : false;

    }
    public void Close()
    {
        if(open)
        {
            open = false;
            LeanTween.alphaCanvas(canvasGroup, 0, duration).setIgnoreTimeScale(true);
        }

    }

}
