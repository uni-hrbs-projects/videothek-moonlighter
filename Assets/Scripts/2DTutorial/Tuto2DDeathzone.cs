using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Tuto2DDeathzone : MonoBehaviour
{
    public Tuto2DWarp tutwarp;
    public UnityEvent afterdeath;
    private void Start()
    {
        tutwarp = FindObjectOfType<Tuto2DWarp>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag=="Character")
        {
            tutwarp.Warp(tutwarp.currentCourse);
            collision.GetComponentInChildren<BatteryManager>().Reset_Battery();
            afterdeath?.Invoke();
        }
    }
}
