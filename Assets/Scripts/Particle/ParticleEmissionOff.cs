using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleEmissionOff : MonoBehaviour
{
    private ParticleSystem _particleSystem;

    private void Awake()
    {
        _particleSystem = GetComponent<ParticleSystem>();
    }

    public void ParticleOff()
    {
        _particleSystem.Stop();
    }

    public void ParticleOn()
    {
        _particleSystem.Play();
    }
}
