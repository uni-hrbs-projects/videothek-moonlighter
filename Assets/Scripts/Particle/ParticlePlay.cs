using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class ParticlePlay : MonoBehaviour
{
    public ParticleSystem _particleSystem { get; set; }

    private void Awake()
    {
        _particleSystem = GetComponent<ParticleSystem>();
    }
    
    public void PlayParticle()
    {
        _particleSystem.Play();
    }

}
