using UnityEngine;

public class SpawnRock : MonoBehaviour
{
    [SerializeField] private Transform spawnpos;
    [SerializeField] private GameObject prefab;

    public void Spawn()
    {
        Instantiate(prefab, spawnpos.position, spawnpos.rotation);
    }
}
