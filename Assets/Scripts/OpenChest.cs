using System;
using UnityEngine;
using UnityEngine.Events;

public class OpenChest : MonoBehaviour
{
    [SerializeField] private Input2D _input2D;
    [SerializeField] private UnityEvent onOpenChest;
    [SerializeField] private Sprite openChest;
    private bool onlyOnce;
    private SpriteRenderer chest;

    private void Awake()
    {
        chest = GetComponent<SpriteRenderer>();
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (onlyOnce)
            return;
        
        if (!other.transform.CompareTag("Character") )
            return;
        
        if (!_input2D.activate) 
            return;
        if(TrackTime.instance!=null)
            TrackTime.instance.Log("Player beat the last Level after " + (Time.time - GameMaster.instance.startTime));
        onOpenChest?.Invoke();
        chest.sprite = openChest;
        onlyOnce = true;
        
        _input2D.activate = false;
    }
}
