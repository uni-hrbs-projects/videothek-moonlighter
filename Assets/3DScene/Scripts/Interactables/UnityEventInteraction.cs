using UnityEngine;
using UnityEngine.Events;

public class UnityEventInteraction : AInteractable
{
    [SerializeField] private UnityEvent eventAction;
    [SerializeField] private UnityEvent onErrorEvent;
    [SerializeField] private UnityEvent onSuccessEvent;
    private Transform objectHolder;

    private void Awake()
    {
        objectHolder = GameObject.FindWithTag("Character").transform.Find("ObjectHolder");
    }
    
    public override void Interaction()
    {
        if (objectHolder.childCount > 0)
        {
            onErrorEvent?.Invoke();
            return;
        }
        
        eventAction?.Invoke();
        onSuccessEvent?.Invoke();
    }
}
