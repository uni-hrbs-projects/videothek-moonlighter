using UnityEngine;
using UnityEngine.Events;

public class TrashItem : AInteractable
{
    private Transform objectHolder;
    [SerializeField] private UnityEvent onSuccess;
    
    private void Awake()
    {
        objectHolder = GameObject.FindWithTag("Character").transform.Find("ObjectHolder");
    }
    
    // Removes item out of characters hand
    public override void Interaction()
    {
        for (int i = 0; i < objectHolder.childCount; i++)
        {
            Destroy(objectHolder.GetChild(i).gameObject);
            onSuccess?.Invoke();
        }
    }
}
