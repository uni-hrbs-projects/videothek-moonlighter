using UnityEngine;
using UnityEngine.Events;

public class GrabItem : AInteractable
{
    [SerializeField] private GameObject itemToCarry;
    [SerializeField] private UnityEvent onErrorEvent;
    [SerializeField] private UnityEvent onSuccessEvent;
    private Transform objectHolder;

    private void Awake()
    {
        objectHolder = GameObject.FindWithTag("Character").transform.Find("ObjectHolder");
    }

    public override void Interaction()
    {
        // For now you can only carry one item
        if (objectHolder.childCount > 0)
        {
            // TODO: Extend with more holdable items or error message
            onErrorEvent?.Invoke();
            return;
        }

        Instantiate(itemToCarry, objectHolder, false);
        onSuccessEvent?.Invoke();
    }
}
