using UnityEngine;

public abstract class AInteractable : MonoBehaviour
{
    public abstract void Interaction();
}
