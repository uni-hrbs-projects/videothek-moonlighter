using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class WaitingInteraction : AInteractable
{
    [SerializeField] private float waitingTime;
    [SerializeField] private UnityEvent onStartInteraction;
    [SerializeField] private UnityEvent onCancelInteraction;
    [SerializeField] private UnityEvent onEndInteraction;
    [SerializeField] private bool onlyWithEmptyHand = true;
    [SerializeField] private UnityEvent onError;

    private Coroutine coroutine;
    private float currentTime = 0;
    private Transform objectHolder;

    private void Awake()
    {
        objectHolder = GameObject.FindWithTag("Character").transform.Find("ObjectHolder");
    }

    public override void Interaction()
    {
        if (coroutine != null)
            return;

        if (onlyWithEmptyHand)
        {
            if (objectHolder.childCount > 0)
            {
                onError?.Invoke();
                return;
            }        
        }
        
        onStartInteraction?.Invoke();
        coroutine = StartCoroutine(Wait());
        CharacterController3D.lockMovement = true;
        CancelEvent.instance.onCancelInvoked += Cancel;
    }

    private IEnumerator Wait()
    {
        CharacterProgress.instance.EnableProgress();

        while (currentTime <= waitingTime)
        {
            yield return null;
            currentTime += Time.deltaTime;
            float progress = Mathf.Clamp01(currentTime / waitingTime);
            CharacterProgress.instance.UpdateProgress(progress);
        }
        onEndInteraction?.Invoke();
        coroutine = null;
        CharacterController3D.lockMovement = false;
        CancelEvent.instance.onCancelInvoked -= Cancel;
        currentTime = 0;
        CharacterProgress.instance.DisableProgress();
    }

    private void Cancel()
    {
        CancelEvent.instance.onCancelInvoked -= Cancel;

        CharacterProgress.instance.DisableProgress();
        StopCoroutine(coroutine);
        onCancelInteraction?.Invoke();
        coroutine = null;
        CharacterController3D.lockMovement = false;
        currentTime = 0;
        Debug.Log("Cancelled");
    }
}
