using System;
using UnityEngine;
using UnityEngine.Events;

public class DeliveryInteraction : AInteractable
{
    public string deliveryItem;
    public int reward;
    [SerializeField] private UnityEvent onDelivery;
    [SerializeField] private UnityEvent onErrorEvent;
    private Transform objectHolder;

    private void Awake()
    {
        objectHolder = GameObject.FindWithTag("Character").transform.Find("ObjectHolder");
    }

    public override void Interaction()
    {
        if (objectHolder.childCount <= 0)
            return;

        for (int i = 0; i < objectHolder.childCount; i++)
        {
            var delivery = objectHolder.GetChild(i).GetComponent<Deliverable>();
            if (delivery == null || delivery.name != deliveryItem) 
                continue;
            
            Destroy(delivery.gameObject);
            onDelivery?.Invoke();
            IncreaseScore();
            return;
        }
        
        onErrorEvent?.Invoke();
    }

    private void IncreaseScore()
    {
        var mood = GetComponent<CustomerMood>();
        float bonus = 0;
        if (mood != null)
            bonus = mood.CurrentMood;
        
        ScoreManager.instance.ChangeScore(Convert.ToInt32(reward * (1 + bonus)));

    }
}
