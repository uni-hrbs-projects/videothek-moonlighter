using System.Collections.Generic;
using UnityEngine;

public class EnableVendingMachineScripts : MonoBehaviour
{
    [SerializeField] private List<VendingMachine> _vendingMachines = new List<VendingMachine>();

    public void EnableMachines()
    {
        foreach (var vendingMachine in _vendingMachines)
        {
            vendingMachine.enabled = true;
        }
    }
}
