using System.Collections.Generic;
using UnityEngine;

public class DirtSpawner : MonoBehaviour
{
    [SerializeField] private List<GameObject> dirtItems = new List<GameObject>();

    public void SpawnDirt()
    {
        int randomIndex = Random.Range(0, dirtItems.Count);
        Vector3 pos = transform.position;
        pos.y = 0;
        Instantiate(dirtItems[randomIndex], pos, Quaternion.identity);
    }
}
