using System;
using UnityEngine;
using UnityEngine.UI;

public class CleanlinessManager : MonoBehaviour
{
    public static CleanlinessManager instance;
    
    public Slider uiElement;
    [SerializeField] private float maxCleanliness = 100;
    [SerializeField] private Image bar;
    [SerializeField] private Color cleanColor;
    [SerializeField] private Color dirtyColor;
    [SerializeField] private float tweenTime = 0.15f;
    [SerializeField] private float scoreMultiplier = 1;
    
    private float currentCleanliness;
    public float CurrentCleanliness => currentCleanliness;

    private int? tweenId = null;

    private void Awake()
    {
        instance = this;
        currentCleanliness = maxCleanliness;
    }
    
    public void ModifyCleanliness(int amount)
    {
        currentCleanliness = Mathf.Clamp((currentCleanliness + amount), 0, maxCleanliness);
        float percent = currentCleanliness / maxCleanliness;

        if(tweenId != null)
            LeanTween.cancel(tweenId.Value);

        if(gameObject != null && uiElement != null)
            tweenId = LeanTween.value(gameObject, Lerp, uiElement.value, percent, tweenTime).id;
    }

    private void Lerp(float val)
    {
        uiElement.value = val;
        bar.color = Color.Lerp(dirtyColor, cleanColor, val);
    }

    public void AwardBonusPoints()
    {
        int bonusPoints = Convert.ToInt32(currentCleanliness * scoreMultiplier);
        
        if (bonusPoints < 30 && ScoreManager.instance.CurrentScore < 30) // award at least 30 Seconds playtime... 
            bonusPoints = 30;
            
        ScoreManager.instance.ChangeScore(bonusPoints);
    }
}
