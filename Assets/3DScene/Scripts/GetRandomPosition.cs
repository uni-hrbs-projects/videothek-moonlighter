using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class GetRandomPosition : MonoBehaviour
{
    private static int childCount;
    private static Transform _transform;
    private static List<Vector3> occupiedPositions = new List<Vector3>();

    private void Awake()
    {
        _transform = transform;
        childCount = _transform.childCount;
    }

    public static Vector3 GetRandomPos()
    {
        int randomChild = Random.Range(0, childCount);
        Vector3 returnPos = _transform.GetChild(randomChild).position;
        if (OccupyCheck(returnPos))
            return GetRandomPos();
        else
            return returnPos;
    }

    private static bool OccupyCheck(Vector3 position)
    {
        if (occupiedPositions.Contains(position))
            return true;
        else
            return false;
    }

    public static void ClearPosition(Vector3 position)
    {
        if (occupiedPositions.Contains(position))
            occupiedPositions.Remove(position);
    }
}
