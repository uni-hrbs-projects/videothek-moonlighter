using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class CharacterController3D : MonoBehaviour
{
    /// <summary>
    /// Flag to allow character movement
    /// </summary>
    public static bool lockMovement = false;
    
    [SerializeField] private Input3D input3D;
    [SerializeField] private float movespeed = 5f;
    [SerializeField] private float dashModifier = 3;
    [SerializeField] private float dashDuration = 0.1f;
    [SerializeField] private float dashCooldown = 0.25f;

    [SerializeField] private UnityEvent onDashStart;
    [SerializeField] private UnityEvent onDashEnd;

    private CharacterController cc;
    private bool moveinput;
    private Vector3 movement;
    private bool isDashing;
    private bool dashOnCooldown;
    private float currentSpeed;
    private Animator animator;
    private Track3DButtons _trackButtons;

    
    private static readonly int Speedpercent = Animator.StringToHash("Blend");

    private void Start()
    {
        _trackButtons = FindObjectOfType<Track3DButtons>();
    }

    private void Awake()
    {
        lockMovement = false;
        cc = GetComponent<CharacterController>();
        animator = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame - check here for User input to not miss it
    private void Update()
    {
        if (lockMovement)
        {
            currentSpeed = 0;
            animator.SetFloat(Speedpercent, 0);
            return;
        }
        
        if (input3D.dash)
        {
            if (!dashOnCooldown)
            {
                if(_trackButtons!=null)
                    _trackButtons.LogButtonPress("Dash");
                StartCoroutine(Dashing());
            }

            input3D.dash = false;
        }

        if (isDashing)
        {
            moveinput = true;
            movement = transform.forward * dashModifier;
        }
        else if (input3D.movement != Vector2.zero) // Calls move-function if there is any Input on our Movementkeys
        {
            moveinput = true;
            
            // Calculate forward and right direction in dependance of the camera viewing direction
            Vector3 forward = Vector3.forward;
            Vector3 right = Vector3.right;

            // Checks for Movementbuttons and calculate total movement
            Vector3 sidewaysmovement = input3D.movement.x * right.normalized;
            Vector3 forwardmovement = input3D.movement.y * forward.normalized;
            movement = forwardmovement + sidewaysmovement;
        }
        
        if (moveinput)
            Move(movement);
        else
        {
            // Reduce movmenetspeed if no move-input
            currentSpeed -= 3 * Time.fixedDeltaTime;
            if (currentSpeed < 0)
                currentSpeed = 0;
        }
        
        animator.SetFloat(Speedpercent, currentSpeed);
    }

    /// <summary>
    /// Character movement if there was user input
    /// </summary>
    /// <param name="totalmovement"></param>
    private void Move(Vector3 totalmovement)
    {
        // Rotate our character towards the movement direction
        Quaternion newRot = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(totalmovement), 15f * Time.deltaTime);
        transform.rotation = newRot;
        
        // Move our Character
        cc.Move((totalmovement + Physics.gravity) * (movespeed * Time.deltaTime));

        // Interpolate currentspeed for smooth run-transition
        currentSpeed += 2 * Time.fixedDeltaTime;
        if (currentSpeed > 1)
            currentSpeed = 1;
        moveinput = false;
    }

    private IEnumerator Dashing()
    {
        isDashing = true;
        dashOnCooldown = true;
        
        onDashStart?.Invoke();
        //animator.SetTrigger("Dash");

        yield return new WaitForSeconds(dashDuration);
        onDashEnd?.Invoke();
        isDashing = false;
        
        yield return new WaitForSeconds(dashCooldown);
        dashOnCooldown = false;
    }

    private void OnDestroy()
    {
        lockMovement = false;
    }
}
