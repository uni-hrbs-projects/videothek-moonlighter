using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

/// <summary>
/// TODO:
/// At the moment this is a randomised spawn-Q
/// For better Gameplay & Balancing option this should be something else, that can be handcrafted
/// But for the Beta Prototype this shall do!
/// </summary>

public class CustomerSpawnQueue : MonoBehaviour
{
    [Header("General")]
    [SerializeField] private Transform entryPosition;
    [SerializeField] private float minDelay;
    [SerializeField] private float maxDelay;

    [Header("Wandering")]
    [SerializeField] private GameObject wanderingPrefab;
    [SerializeField] private int minPoints;
    [SerializeField] private int maxPoints;
    
    [Header("Quest")]
    [SerializeField] private GameObject questPrefab;
    
    [Header("Mixed")]
    [SerializeField] private List<GameObject> mixedPrefabs = new List<GameObject>();

    private int randomMin = 0;
    private int randomMax = 101;
    private bool wanderingSpawned;
    private bool questSpawned;
    private int nextSpawn = 0;
    
    // Start is called before the first frame update
    private void Start()
    {
        // Old SpawnCustomer(); 
        StartCoroutine(WaitForNewSpawn(0.1f));
    }

    private IEnumerator WaitForNewSpawn(float delay)
    {
        yield return new WaitForSeconds(delay);
        //SpawnCustomer(); // Old Way
        NewCustomerSpawner();
    }

    private void NewCustomerSpawner()
    {
        Instantiate(mixedPrefabs[nextSpawn], entryPosition.position, Quaternion.identity);
        nextSpawn += 1;
        if (nextSpawn >= mixedPrefabs.Count)
            nextSpawn = 0;
        float randomDelay = Random.Range(minDelay, maxDelay);
        StartCoroutine(WaitForNewSpawn(randomDelay));
    }
    
    private void SpawnCustomer()
    {
        int randomCustomer = Random.Range(randomMin, randomMax);

        // Skew the chances towards the other type on each spawn of a type
        if (randomCustomer <= 50)
        {
            GameObject customer = Instantiate(wanderingPrefab, entryPosition.position, Quaternion.identity);
            customer.GetComponent<WanderingCustomer>().targetPointCount = Random.Range(minPoints, maxPoints);
            randomMin += 25;
            if (randomMax > 100)
                randomMax = 100;
            wanderingSpawned = true;
        }
        else
        {
            Instantiate(questPrefab, entryPosition.position, Quaternion.identity);
            randomMax -= 25;
            if (randomMax < 1)
                randomMax = 1;
            questSpawned = true;
        }

        if (wanderingSpawned && questSpawned)
        {
            // Reset odds
            wanderingSpawned = questSpawned = false;
            randomMin = 0;
            randomMax = 101;
        }
        
        float randomDelay = Random.Range(minDelay, maxDelay);
        StartCoroutine(WaitForNewSpawn(randomDelay));
    }
}
