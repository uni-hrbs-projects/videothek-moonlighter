using UnityEngine;

public class Tutorial : MonoBehaviour
{
    [SerializeField] private BoolSo tutorialDone;

    // Start is called before the first frame update
    void Awake()
    {
        if (!tutorialDone.boolean)
        {
            Time.timeScale = 0;
            Debug.Log("Set Time Scale to 0");
        }
    }
}
