using TMPro;
using UnityEngine;
using UnityEngine.Events;


/// <summary>
/// Source for this Script:
/// https://gamedevbeginner.com/how-to-make-countdown-timer-in-unity-minutes-seconds/
/// </summary>
public class LevelTimer : MonoBehaviour
{
    [SerializeField] private float timeRemaining = 10;
    [SerializeField] private TextMeshProUGUI timeText;
    [SerializeField] private UnityEvent onTimerEnd;

    [HideInInspector] public bool timerIsRunning = false;

    private void Start()
    {
        // Starts the timer automatically
        timerIsRunning = true;
    }

    void Update()
    {
        if (timerIsRunning)
        {
            if (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
                DisplayTime(timeRemaining);
            }
            else
            {
                timeRemaining = 0;
                timerIsRunning = false;
                onTimerEnd?.Invoke();
                timerIsRunning = false;
            }
        }
    }

    void DisplayTime(float timeToDisplay)
    {
        timeToDisplay += 1;

        float minutes = Mathf.FloorToInt(timeToDisplay / 60); 
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);

        timeText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }
}
