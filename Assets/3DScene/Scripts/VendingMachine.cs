using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class VendingMachine : MonoBehaviour
{
    [SerializeField] private int passiveIncome = 2;
    [SerializeField] private float incomePerTimeframe = 1;
    [SerializeField] private float minTimeToBreak = 7.5f;
    [SerializeField] private float maxTimeToBreak = 20f;
    [SerializeField] private float repairTime = 4f;
    [SerializeField] private GameObject brokenIndicator;
    [SerializeField] private Slider progressSlider;
    
    private bool isWorking = true;
    private bool isRepairing = false;
    private float timeGoneBy;

    private void Awake()
    {
        if(enabled)
            StartCoroutine(WaitRandomTime());
    }

    private IEnumerator WaitRandomTime()
    {
        float waitTime = Random.Range(minTimeToBreak, maxTimeToBreak);
        yield return new WaitForSeconds(waitTime);
        BreakMachine();
    }

    // Update is called once per frame
    private void Update()
    {
        if (isRepairing)
        {
            timeGoneBy += Time.deltaTime;
            progressSlider.value = Mathf.Clamp01(timeGoneBy / repairTime);
        }
        
        if (!isWorking)
            return;
        
        timeGoneBy += Time.deltaTime;
        
        if(timeGoneBy >= incomePerTimeframe)
        {
            ScoreManager.instance.ChangeScore(passiveIncome);
            timeGoneBy = 0;
        }    
    }

    public void BreakMachine()
    {
        isWorking = false;

        // Indicate the need of fixing and make interactable
        gameObject.tag = "Interactable";
        brokenIndicator.SetActive(true);
    }

    public void RepairMachine()
    {
        if (isRepairing)
            return;
        
        if(!isWorking)
            StartCoroutine(RepairCycle());
    }

    private IEnumerator RepairCycle()
    {
        isRepairing = true;
        brokenIndicator.SetActive(false);
        
        progressSlider.gameObject.SetActive(true);
        timeGoneBy = 0;

        yield return new WaitForSeconds(repairTime);

        timeGoneBy = 0;
        progressSlider.gameObject.SetActive(false);

        gameObject.tag = "Untagged";
        isWorking = true;
        StartCoroutine(WaitRandomTime());
        isRepairing = false;
    }
}
