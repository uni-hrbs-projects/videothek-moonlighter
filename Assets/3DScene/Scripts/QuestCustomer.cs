using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

[RequireComponent(typeof(NavMeshAgent))]
public class QuestCustomer : MonoBehaviour
{
    [SerializeField] private UnityEvent onTargetReached;
    
    private Animator animator;
    private NavMeshAgent agent;
    private static readonly int Speed = Animator.StringToHash("Speed");
    private Queue<Vector3> targetPoints = new Queue<Vector3>();
    private bool isIdling;

    private Vector3 currentTargetPos;

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();

        // Create a path
        targetPoints.Enqueue(GetRandomPosition.GetRandomPos());

        // Add Exit point 
        targetPoints.Enqueue(GameObject.Find("ExitPoint").transform.position);
        
        // Send to first target
        currentTargetPos = targetPoints.Dequeue();
        agent.SetDestination(currentTargetPos);
    }

    private void Update()
    {
        // animate speed
        float agentVelocity = agent.velocity.magnitude / agent.speed;
        animator.SetFloat(Speed, agentVelocity);

        if (isIdling)
            return;
        
        // Check if agent reached destination
        float dist = agent.remainingDistance;
        if (float.IsPositiveInfinity(dist) || agent.pathStatus != NavMeshPathStatus.PathComplete || agent.remainingDistance != 0) 
            return;

        Debug.Log("Reached target");
        
        if (targetPoints.Count <= 0)
        {
            // Reached Exit
            Destroy(gameObject);
        }
        else
        {
            isIdling = true;
            onTargetReached?.Invoke();
        }
    }

    public void MoveToExit()
    {
        GetRandomPosition.ClearPosition(currentTargetPos);
        currentTargetPos = targetPoints.Dequeue();
        agent.SetDestination(currentTargetPos);
        StartCoroutine(DelayedActive());
    }

    private IEnumerator DelayedActive()
    {
        // Need to wait a frame otherwise agent still thinks its at destination
        yield return null;
        isIdling = false;
    }
    
}
