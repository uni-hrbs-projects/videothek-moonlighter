using System;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager instance;

    [SerializeField] private TextMeshProUGUI scoreUi;
    [SerializeField] private IntergerSo persistentScore;
    [SerializeField] private UnityEvent onIncrease;
    
    private int currentScore = 0;
    private int multiplier = 1;
    public int CurrentScore => currentScore;


    private void Awake()
    {
        instance = this;
        if (FindObjectOfType<GameMaster>() != null && FindObjectOfType<GameMaster>().coinMultiplier)
        {
            multiplier = 2;
        }
        else
        {
            multiplier = 1;
        }
    }

    public void ChangeScore(int amount)
    {
        if(amount > 2)
            onIncrease?.Invoke();
        currentScore += (amount*multiplier);
        if (amount < 0)
            amount = 0;

        UpdateUi();
    }

    private void UpdateUi()
    {
        scoreUi.text = currentScore + "";
    }

    private void OnDestroy()
    {
        persistentScore.integer = currentScore;
    }
}
