using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
using Random = UnityEngine.Random;

[RequireComponent(typeof(NavMeshAgent))]
public class MixedCustomer : MonoBehaviour
{
    [SerializeField] private UnityEvent onDirtQuest;
    [SerializeField] private UnityEvent onRequestQuest;
    [SerializeField] private float idleTime = 0.5f;
    
    private Animator animator;
    private NavMeshAgent agent;
    private static readonly int Speed = Animator.StringToHash("Speed");
    private Queue<Vector3> targetPoints = new Queue<Vector3>();
    private bool isIdling;
    private Vector3 currentTargetPos;
    
    private int targetPointCount = 2;
    private int minPoints = 2;
    private int maxPoints = 4;
    private bool madeDirt;
    private bool madeRequest;

    private void Awake()
    {
        // Sets target points
        minPoints = Random.Range(minPoints, maxPoints + 1);
    }

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();

        // Create a path
        for (int i = 0; i < targetPointCount; i++)
        {
            targetPoints.Enqueue(GetRandomPosition.GetRandomPos());
        }
        
        // Add Exit point 
        targetPoints.Enqueue(GameObject.Find("ExitPoint").transform.position);
        
        // Send to first target
        currentTargetPos = targetPoints.Dequeue();
        agent.SetDestination(currentTargetPos);
    }

    private void Update()
    {
        // animate speed
        float agentVelocity = agent.velocity.magnitude / agent.speed;
        animator.SetFloat(Speed, agentVelocity);

        if (isIdling)
            return;
        
        // Check if agent reached destination
        float dist = agent.remainingDistance;
        if (float.IsPositiveInfinity(dist) || agent.pathStatus != NavMeshPathStatus.PathComplete || agent.remainingDistance != 0) 
            return;
        
        if (targetPoints.Count <= 0)
        {
            // Reached Exit
            Destroy(gameObject);
            return;
        }

        DetermineBehaviour();
    }

    private void DetermineBehaviour()
    {
        if (madeDirt && madeRequest)
            madeDirt = madeRequest = false;
        
        if (!madeDirt && !madeRequest)
        {
            int random = Random.Range(0, 2);
            if (random == 0)
                SetRequest();
            else
                StartCoroutine(DropDirt());
        }
        else
        {
            if(madeDirt)
                SetRequest();
            else
                StartCoroutine(DropDirt());
        }
    }

    private IEnumerator DropDirt()
    {
        madeDirt = true;
        
        isIdling = true;
        yield return new WaitForSeconds(idleTime);

        onDirtQuest?.Invoke();

        GetRandomPosition.ClearPosition(currentTargetPos);
        currentTargetPos = targetPoints.Dequeue();
        agent.SetDestination(currentTargetPos);
        
        isIdling = false;
    }
    
    private void SetRequest()
    {
        madeRequest = true;
        
        isIdling = true;
        onRequestQuest?.Invoke();
    }
    
    public void MoveToNext()
    {
        GetRandomPosition.ClearPosition(currentTargetPos);
        currentTargetPos = targetPoints.Dequeue();
        agent.SetDestination(currentTargetPos);
        StartCoroutine(DelayedActive());
    }

    private IEnumerator DelayedActive()
    {
        // Need to wait a frame otherwise agent still thinks its at destination
        yield return null;
        isIdling = false;
    }
}
