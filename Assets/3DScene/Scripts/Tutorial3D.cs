using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Tutorial3D : MonoBehaviour
{
    [SerializeField] private CanvasFader _canvasFader;
    [SerializeField] private List<UnityEvent> tutorialSteps = new List<UnityEvent>();
    [SerializeField] private Input3D input3D;
    [SerializeField] private float activationTime = 0.25f;
    [SerializeField] private float afterActiveDelay = 0.25f;
    [SerializeField] private Image fill;

    private int currentStep;
    private float currentTime;
    private bool noInput;

    private void Update()
    {
        if (noInput)
            return;
        
        if (input3D.next)
            currentTime += Time.deltaTime;
        else
            currentTime -= Time.deltaTime;
        
        currentTime = Mathf.Clamp(currentTime, 0, activationTime);
        float percent = Mathf.Clamp01(currentTime / activationTime);
        
        fill.fillAmount = percent;
        if(percent == 1)
            Next();
    }

    private void Next()
    {
        fill.fillAmount = 0;
        currentTime = 0;

        if (currentStep >= tutorialSteps.Count)
        {
            _canvasFader.FadeOut();
        }
        else
        {
            tutorialSteps[currentStep].Invoke();
        }
        
        currentStep += 1;

        StartCoroutine(NoPressDelay());
    }

    private IEnumerator NoPressDelay()
    {
        noInput = true;
        yield return new WaitForSeconds(afterActiveDelay);
        noInput = false;
    }
}
