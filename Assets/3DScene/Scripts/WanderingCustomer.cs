using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

[RequireComponent(typeof(NavMeshAgent))]
public class WanderingCustomer : MonoBehaviour
{
    public int targetPointCount;
    [SerializeField] private UnityEvent onTargetReached;
    [SerializeField] private float idleTime = 0.5f;
    
    private Animator animator;
    private NavMeshAgent agent;
    private static readonly int Speed = Animator.StringToHash("Speed");
    private Queue<Vector3> targetPoints = new Queue<Vector3>();
    private bool isIdling;
    
    private Vector3 currentTargetPos;

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();

        int lastIndex = -1; // Ensure to not visit the same point twice in a row
        // Create a path
        for (int i = 0; i < targetPointCount; i++)
        {
            targetPoints.Enqueue(GetRandomPosition.GetRandomPos());
        }
        
        // Add Exit point 
        targetPoints.Enqueue(GameObject.Find("ExitPoint").transform.position);
        
        // Send to first target
        currentTargetPos = targetPoints.Dequeue();
        agent.SetDestination(currentTargetPos);
    }

    private void Update()
    {
        // animate speed
        float agentVelocity = agent.velocity.magnitude / agent.speed;
        animator.SetFloat(Speed, agentVelocity);

        if (isIdling)
            return;
        
        // Check if agent reached destination
        float dist = agent.remainingDistance;
        if (float.IsPositiveInfinity(dist) || agent.pathStatus != NavMeshPathStatus.PathComplete || agent.remainingDistance != 0) 
            return;
        
        if (targetPoints.Count <= 0)
        {
            // Reached Exit
            Destroy(gameObject);
            return;
        }

        StartCoroutine(SetNewDest());
    }

    private IEnumerator SetNewDest()
    {
        isIdling = true;
        yield return new WaitForSeconds(idleTime);
        
        onTargetReached?.Invoke();

        GetRandomPosition.ClearPosition(currentTargetPos);
        currentTargetPos = targetPoints.Dequeue();
        agent.SetDestination(currentTargetPos);
        
        isIdling = false;
    }
}
