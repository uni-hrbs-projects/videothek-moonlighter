using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomerMood : MonoBehaviour
{
    public float CurrentMood { get; private set; } = 1;

    [SerializeField] private float maxWaitTime = 5f;
    [SerializeField] private List<Sprite> moodIcons = new List<Sprite>();
    [SerializeField] private Image moodDisplay;
    
    private bool isWaiting;
    private float waitedTime;

    // TODO: Add visual Feedback for current mood!
    
    public void StartTracking()
    {
        isWaiting = true;
    }

    private void Update()
    {
        if (!isWaiting)
            return;

        float cleanlinesModifier = 1 + 1 - CleanlinessManager.instance.uiElement.value;
        waitedTime += Time.deltaTime * cleanlinesModifier;
        float percent = Mathf.Clamp01(waitedTime / maxWaitTime);
        CurrentMood = 1 - percent;
        
        UpdateIcon();
    }

    private void UpdateIcon()
    {
        int index = Convert.ToInt32(CurrentMood / 0.2f);
        if (index >= moodIcons.Count)
        {
            index = moodIcons.Count - 1;
        }

        if (moodDisplay.sprite != moodIcons[index])
            moodDisplay.sprite = moodIcons[index];
    }

    public void StopTracking()
    {
        isWaiting = false;
    }
}
