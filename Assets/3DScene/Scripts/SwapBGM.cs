using UnityEngine;

public class SwapBGM : MonoBehaviour
{
    private SoundManager soundManager;

    [SerializeField] private AudioClip bgm;
    
    private void Awake()
    {
        soundManager = FindObjectOfType<SoundManager>();

        if (soundManager == null)
            return;
        
        soundManager.StopBGM();
        
        if(bgm != null)
            soundManager.PlayBGM(bgm);
    }
}
