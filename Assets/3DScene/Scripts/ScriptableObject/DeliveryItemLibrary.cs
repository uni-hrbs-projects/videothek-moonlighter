using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/DeliveryLibrary", order = 1)]
public class DeliveryItemLibrary : ScriptableObject
{
    public List<DeliveryItem> library = new List<DeliveryItem>();
}

[System.Serializable]
public struct DeliveryItem
{
    public string id;
    public Sprite icon;
    public int reward;
}
