using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SetTutorialTexts : MonoBehaviour
{
    [SerializeField] private List<string> texts = new List<string>();
    [SerializeField] private Transform extraIndicatorParent;
    private TextMeshProUGUI textfield;

    private void Awake()
    {
        textfield = GetComponent<TextMeshProUGUI>();
    }

    public void SetText(int index)
    {
        textfield.text = texts[index];

        foreach (Transform child in extraIndicatorParent)
        {
            child.gameObject.SetActive(false);
        }
        
        extraIndicatorParent.GetChild(index).gameObject.SetActive(true);
    }
}
