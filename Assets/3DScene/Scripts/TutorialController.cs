using UnityEngine;

public class TutorialController : MonoBehaviour
{
    private int index = 0;
    private GameObject[] tutorials;
    [SerializeField] private Input3D input3D;
    [SerializeField] private BoolSo tutorialDone;


    private int countTutorials;
    // Start is called before the first frame update
    void Start()
    {
        tutorials = GameObject.FindGameObjectsWithTag("Tutorial");
        countTutorials = tutorials.Length;
        if (!tutorialDone.boolean)
        {
            tutorials[0].gameObject.transform.GetChild(0).gameObject.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (input3D.interact && !tutorialDone.boolean)
        {
            input3D.interact = false;
            if (index < (countTutorials-1))
            {
                tutorials[index].gameObject.transform.GetChild(0).gameObject.SetActive(false);
                index += 1;
                tutorials[index].gameObject.transform.GetChild(0).gameObject.SetActive(true);
            }
            else
            {
                tutorials[index].gameObject.transform.GetChild(0).gameObject.SetActive(false);
                Time.timeScale = 1;
                tutorialDone.boolean = true;
            }
        }
    }
}
