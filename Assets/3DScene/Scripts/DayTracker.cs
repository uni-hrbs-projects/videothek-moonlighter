using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayTracker : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if(FindObjectOfType<TrackDayLevels>()!=null)
            FindObjectOfType<TrackDayLevels>().Log();
    }
}
