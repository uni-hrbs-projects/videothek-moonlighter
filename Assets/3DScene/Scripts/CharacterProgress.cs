using UnityEngine;
using UnityEngine.UI;

public class CharacterProgress : MonoBehaviour
{
    [SerializeField] private Slider progressSlider;
    public static CharacterProgress instance;

    private void Awake()
    {
        instance = this;
    }

    public void EnableProgress()
    {
        progressSlider.value = 0;
        progressSlider.gameObject.SetActive(true);
    }

    public void UpdateProgress(float value)
    {
        progressSlider.value = value;
    }

    public void DisableProgress()
    {
        progressSlider.gameObject.SetActive(false);
    }
}
