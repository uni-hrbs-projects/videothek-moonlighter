using System;
using UnityEngine;

public class CancelEvent : MonoBehaviour
{
    public static CancelEvent instance;
    
    [SerializeField] private Input3D input3D;

    public event Action onCancelInvoked;

    private void Awake()
    {
        instance = this;
    }

    protected virtual void OnOnCancelInvoked()
    {
        onCancelInvoked?.Invoke();
    }

    private void Update()
    {
        if (!input3D.cancel)
            return;

        input3D.cancel = false;
        OnOnCancelInvoked();
    }
}
