using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField] private GameObject door;
    [SerializeField] private float closed;
    [SerializeField] private float open;
    [SerializeField] private float tweenTime;

    private int counter;
    private int? id = null;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Character"))
            return;

        counter++;
        OpenDoor();
    }

    private void OpenDoor()
    {
        if(id != null)
            LeanTween.cancel(id.Value);
        
        id = LeanTween.rotateY(door, open, tweenTime).setEaseSpring().setOnComplete(() => id = null).id;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Character"))
            return;

        counter--;
        if(counter <= 0)
            CloseDoor();
    }

    private void CloseDoor()
    {
        if(id != null)
            LeanTween.cancel(id.Value);
        
        id = LeanTween.rotateY(door, closed, tweenTime).setEaseSpring().setOnComplete(() => id = null).id;
    }
}
