using UnityEngine;
using UnityEngine.InputSystem;


public class Forward3DInput : MonoBehaviour
{
    [SerializeField] private Input3D input;

    public void OnDash(InputAction.CallbackContext value)
    {
        switch (value.phase)
        {
            case InputActionPhase.Started:
                input.dash = true;
                break;
        }
    }
    
    public void OnInteraction(InputAction.CallbackContext value)
    {
        switch (value.phase)
        {
            case InputActionPhase.Started:
                input.interact = true;
                break;
        }
    }
    
    public void OnCancel(InputAction.CallbackContext value)
    {
        switch (value.phase)
        {
            case InputActionPhase.Started:
                input.cancel = true;
                break;
        }
    }
    
    public void OnMovement(InputAction.CallbackContext value)
    {
        input.movement = value.ReadValue<Vector2>();
    }
    
    public void OnNext(InputAction.CallbackContext value)
    {
        input.next = value.ReadValueAsButton();
    }
    
}
