using System.Collections.Generic;
using UnityEngine;

public class InteractableDetection : MonoBehaviour
{
    [SerializeField] private Input3D input3D;
    [SerializeField] private BoolSo tutorialDone;
    [SerializeField] private float inputBuffer = 0.15f;
    
    private List<GameObject> interactableInRange = new List<GameObject>();
    private GameObject currentHighlight = null;
    private float pressedAt = -1;
    private Track3DButtons _trackButtons;

    private void Start()
    {
        Physics.IgnoreCollision(GetComponent<Collider>(), transform.parent.GetComponent<Collider>());
        _trackButtons = FindObjectOfType<Track3DButtons>();
    }

    private void Update()
    {
        if (input3D.interact)
        {
            pressedAt = Time.time;
            input3D.interact = false;
        }

        if (currentHighlight == null)
            return;

        var interaction = currentHighlight.GetComponent<AInteractable>();
        if (interaction == null)
            return;
        
        if(Time.time - pressedAt <= inputBuffer)
        {
            if(_trackButtons!=null)
                _trackButtons.LogButtonPress("Interact");
            interaction.Interaction();
            pressedAt = -1;
        }    
    }
    

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Interactable"))
            return;
        
        interactableInRange.Add(other.gameObject);
        HighlightNewSelection();

    }

    private void OnTriggerExit(Collider other)
    {
        if (!other.CompareTag("Interactable") && other.GetComponent<VendingMachine>() == null)
            return;
        
        interactableInRange.Remove(other.gameObject);
        HighlightNewSelection();
    }

    private void HighlightNewSelection()
    {
        // Remove last highlight
        if(currentHighlight != null)
            currentHighlight.GetComponent<AHighlightable>().Lowlight();

        if (interactableInRange.Count == 0)
        {
            currentHighlight = null;
            return;
        }
        if (interactableInRange.Count == 1)
        {
            currentHighlight = interactableInRange[0];
            currentHighlight.GetComponent<AHighlightable>().Highlight();
            return;
        }

        currentHighlight = GetInFront();
        currentHighlight.GetComponent<AHighlightable>().Highlight();
    }

    private GameObject GetInFront()
    {
        int winnerIndex = 0;
        float smallestDot = Mathf.Abs(Vector3.Dot(transform.parent.forward.normalized, (interactableInRange[0].transform.position - transform.parent.position).normalized));
        for (int i = 1; i < interactableInRange.Count; i++)
        {
            float newDot = Mathf.Abs(Vector3.Dot(transform.parent.forward.normalized, (interactableInRange[0].transform.position - transform.parent.position).normalized));
            if (newDot < smallestDot)
            {
                winnerIndex = i;
                smallestDot = newDot;
            }
        }

        return interactableInRange[winnerIndex];
    }

    public void RemoveDestroyedInteraction(GameObject removed)
    {
        interactableInRange.Remove(removed);
        currentHighlight = null;
        HighlightNewSelection();
    }
}
