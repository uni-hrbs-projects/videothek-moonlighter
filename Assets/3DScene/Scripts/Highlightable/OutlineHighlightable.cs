using UnityEngine;
using Outline3D;

[RequireComponent(typeof(Outline))]
public class OutlineHighlightable : AHighlightable
{
    [SerializeField] private Color outlineColor = Color.yellow;
    [SerializeField] private float outlineThickness = 5;
    private Outline outline;

    private void Awake()
    {
        outline = GetComponent<Outline>();
        outline.OutlineColor = outlineColor;
        outline.OutlineWidth = outlineThickness;
    }

    public override void Highlight()
    {
        outline.enabled = true;
    }

    public override void Lowlight()
    {
        outline.enabled = false;
    }
}
