using UnityEngine;

public abstract class AHighlightable : MonoBehaviour
{
    public abstract void Highlight();

    public abstract void Lowlight(); // LOL I dont know the oppsite :D
}
