using UnityEngine;
using UnityEngine.UI;

public class EnableQuest : MonoBehaviour
{
    [SerializeField] private DeliveryItemLibrary library;
    [SerializeField] private Image deliveryCue;
    [SerializeField] private GameObject ui;
    
    public void StartQuest()
    {
        int randomItem = Random.Range(0, library.library.Count);
        var deliveryInteraction = GetComponent<DeliveryInteraction>();
        deliveryInteraction.reward = library.library[randomItem].reward;
        deliveryInteraction.deliveryItem = library.library[randomItem].id;
        deliveryCue.sprite = library.library[randomItem].icon;
        ui.SetActive(true);
        gameObject.tag = "Interactable";
    }
}
