using UnityEngine;

public class DestroyGO : MonoBehaviour
{
    [SerializeField] private bool isDirt;
    [SerializeField] private GameObject toDestroy;

    public void DestroyThis()
    {
        if (isDirt)
        {
            InteractableDetection interactableDetection = GameObject.FindWithTag("Character").GetComponentInChildren<InteractableDetection>();
            interactableDetection.RemoveDestroyedInteraction(toDestroy);
        }
        
        Destroy(toDestroy);
    }
}
