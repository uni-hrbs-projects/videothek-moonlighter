using UnityEngine;

public class Dirt : MonoBehaviour
{
    [SerializeField] private int dirtLevel;

    private void Start()
    {
        if(CleanlinessManager.instance != null)
            CleanlinessManager.instance.ModifyCleanliness(-dirtLevel);
    }

    private void OnDestroy()
    {
        if(CleanlinessManager.instance != null)
            CleanlinessManager.instance.ModifyCleanliness(+dirtLevel);
    }
}
