using UnityEngine;

public class UIFollowTargetPosition : MonoBehaviour
{
    [SerializeField] private Transform toFollow;
    private Camera mainCam;

    private void Awake()
    {
        mainCam = Camera.main;
    }

    private void LateUpdate()
    {
        Vector3 screenPos = mainCam.WorldToScreenPoint(toFollow.position);
        transform.position = screenPos;
    }
}
