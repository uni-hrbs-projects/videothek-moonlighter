// GENERATED AUTOMATICALLY FROM 'Assets/Controls/3DInput.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @_3DInput : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @_3DInput()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""3DInput"",
    ""maps"": [
        {
            ""name"": ""Player3D"",
            ""id"": ""f7cd1012-0d5f-44df-bb05-05214f37320a"",
            ""actions"": [
                {
                    ""name"": ""Interact"",
                    ""type"": ""Button"",
                    ""id"": ""10025bc4-501e-4dc8-b45a-17637deafb2b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Dash"",
                    ""type"": ""Button"",
                    ""id"": ""b6f8c0a9-a643-48bd-86e0-57960885d21f"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Movement"",
                    ""type"": ""Value"",
                    ""id"": ""6def6c24-73fc-44df-a8ab-9f2a3e2673ab"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Cancel"",
                    ""type"": ""Button"",
                    ""id"": ""3ece96a8-d328-489f-b5ab-6978163dd7d6"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Next"",
                    ""type"": ""Button"",
                    ""id"": ""8849effa-2ba1-4de0-9b64-5e9fc9afcc45"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""f1b3e401-1941-4da7-819f-135b4e64d3db"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f75aabc2-29ce-45b9-ab6e-3ae3ea84b210"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9a43b0eb-339c-45ed-a901-5a2a4b4718bb"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Dash"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5911c834-5a4f-4212-9ccb-59a4586635af"",
                    ""path"": ""<Keyboard>/shift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Dash"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""2D Vector"",
                    ""id"": ""04d92752-208a-4000-9f34-9a474b5eb312"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""7864c9e7-b343-4b7e-bdc9-355380f98c08"",
                    ""path"": ""<Gamepad>/leftStick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""up"",
                    ""id"": ""fad755c9-6bd8-4747-b8d8-3bca77adca1a"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""9aab7f72-bc1f-4807-aff9-067b1b2e308e"",
                    ""path"": ""<Gamepad>/leftStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""f0f3e2c9-cd91-4422-b876-6995b0df94dc"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""7c2c7a0a-07ab-4d8f-b2ef-ee050b622661"",
                    ""path"": ""<Gamepad>/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""c7c4fd27-19e4-4ec4-8ade-4a863ed5157d"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""33faf49d-b671-4b9e-a070-c4d4f73f447b"",
                    ""path"": ""<Gamepad>/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""9ac2f5b6-7d90-4a41-9f8e-5bbd4844ebb8"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""2e479cda-c66f-463a-92d1-7c3438821691"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Cancel"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""fb48287c-ccce-4bab-8e70-39549829e5b6"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Cancel"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3d5692ed-7b20-42fc-a3e4-04f01aa535fd"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Next"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""25f31984-1233-48cc-9b4c-fc60d529622f"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Next"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Player3D
        m_Player3D = asset.FindActionMap("Player3D", throwIfNotFound: true);
        m_Player3D_Interact = m_Player3D.FindAction("Interact", throwIfNotFound: true);
        m_Player3D_Dash = m_Player3D.FindAction("Dash", throwIfNotFound: true);
        m_Player3D_Movement = m_Player3D.FindAction("Movement", throwIfNotFound: true);
        m_Player3D_Cancel = m_Player3D.FindAction("Cancel", throwIfNotFound: true);
        m_Player3D_Next = m_Player3D.FindAction("Next", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Player3D
    private readonly InputActionMap m_Player3D;
    private IPlayer3DActions m_Player3DActionsCallbackInterface;
    private readonly InputAction m_Player3D_Interact;
    private readonly InputAction m_Player3D_Dash;
    private readonly InputAction m_Player3D_Movement;
    private readonly InputAction m_Player3D_Cancel;
    private readonly InputAction m_Player3D_Next;
    public struct Player3DActions
    {
        private @_3DInput m_Wrapper;
        public Player3DActions(@_3DInput wrapper) { m_Wrapper = wrapper; }
        public InputAction @Interact => m_Wrapper.m_Player3D_Interact;
        public InputAction @Dash => m_Wrapper.m_Player3D_Dash;
        public InputAction @Movement => m_Wrapper.m_Player3D_Movement;
        public InputAction @Cancel => m_Wrapper.m_Player3D_Cancel;
        public InputAction @Next => m_Wrapper.m_Player3D_Next;
        public InputActionMap Get() { return m_Wrapper.m_Player3D; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(Player3DActions set) { return set.Get(); }
        public void SetCallbacks(IPlayer3DActions instance)
        {
            if (m_Wrapper.m_Player3DActionsCallbackInterface != null)
            {
                @Interact.started -= m_Wrapper.m_Player3DActionsCallbackInterface.OnInteract;
                @Interact.performed -= m_Wrapper.m_Player3DActionsCallbackInterface.OnInteract;
                @Interact.canceled -= m_Wrapper.m_Player3DActionsCallbackInterface.OnInteract;
                @Dash.started -= m_Wrapper.m_Player3DActionsCallbackInterface.OnDash;
                @Dash.performed -= m_Wrapper.m_Player3DActionsCallbackInterface.OnDash;
                @Dash.canceled -= m_Wrapper.m_Player3DActionsCallbackInterface.OnDash;
                @Movement.started -= m_Wrapper.m_Player3DActionsCallbackInterface.OnMovement;
                @Movement.performed -= m_Wrapper.m_Player3DActionsCallbackInterface.OnMovement;
                @Movement.canceled -= m_Wrapper.m_Player3DActionsCallbackInterface.OnMovement;
                @Cancel.started -= m_Wrapper.m_Player3DActionsCallbackInterface.OnCancel;
                @Cancel.performed -= m_Wrapper.m_Player3DActionsCallbackInterface.OnCancel;
                @Cancel.canceled -= m_Wrapper.m_Player3DActionsCallbackInterface.OnCancel;
                @Next.started -= m_Wrapper.m_Player3DActionsCallbackInterface.OnNext;
                @Next.performed -= m_Wrapper.m_Player3DActionsCallbackInterface.OnNext;
                @Next.canceled -= m_Wrapper.m_Player3DActionsCallbackInterface.OnNext;
            }
            m_Wrapper.m_Player3DActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Interact.started += instance.OnInteract;
                @Interact.performed += instance.OnInteract;
                @Interact.canceled += instance.OnInteract;
                @Dash.started += instance.OnDash;
                @Dash.performed += instance.OnDash;
                @Dash.canceled += instance.OnDash;
                @Movement.started += instance.OnMovement;
                @Movement.performed += instance.OnMovement;
                @Movement.canceled += instance.OnMovement;
                @Cancel.started += instance.OnCancel;
                @Cancel.performed += instance.OnCancel;
                @Cancel.canceled += instance.OnCancel;
                @Next.started += instance.OnNext;
                @Next.performed += instance.OnNext;
                @Next.canceled += instance.OnNext;
            }
        }
    }
    public Player3DActions @Player3D => new Player3DActions(this);
    public interface IPlayer3DActions
    {
        void OnInteract(InputAction.CallbackContext context);
        void OnDash(InputAction.CallbackContext context);
        void OnMovement(InputAction.CallbackContext context);
        void OnCancel(InputAction.CallbackContext context);
        void OnNext(InputAction.CallbackContext context);
    }
}
