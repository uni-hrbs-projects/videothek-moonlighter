// GENERATED AUTOMATICALLY FROM 'Assets/Controls/MousePosition.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @MousePosition : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @MousePosition()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""MousePosition"",
    ""maps"": [
        {
            ""name"": ""MousePos"",
            ""id"": ""4f23a572-bbae-43f4-a072-acb3063445e2"",
            ""actions"": [
                {
                    ""name"": ""MouseMovement"",
                    ""type"": ""Value"",
                    ""id"": ""45c5be07-42e8-45a0-87a8-f2fbecf136ad"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""1eceed2f-7443-41ac-9042-70d2cc5025fb"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MouseMovement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // MousePos
        m_MousePos = asset.FindActionMap("MousePos", throwIfNotFound: true);
        m_MousePos_MouseMovement = m_MousePos.FindAction("MouseMovement", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // MousePos
    private readonly InputActionMap m_MousePos;
    private IMousePosActions m_MousePosActionsCallbackInterface;
    private readonly InputAction m_MousePos_MouseMovement;
    public struct MousePosActions
    {
        private @MousePosition m_Wrapper;
        public MousePosActions(@MousePosition wrapper) { m_Wrapper = wrapper; }
        public InputAction @MouseMovement => m_Wrapper.m_MousePos_MouseMovement;
        public InputActionMap Get() { return m_Wrapper.m_MousePos; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MousePosActions set) { return set.Get(); }
        public void SetCallbacks(IMousePosActions instance)
        {
            if (m_Wrapper.m_MousePosActionsCallbackInterface != null)
            {
                @MouseMovement.started -= m_Wrapper.m_MousePosActionsCallbackInterface.OnMouseMovement;
                @MouseMovement.performed -= m_Wrapper.m_MousePosActionsCallbackInterface.OnMouseMovement;
                @MouseMovement.canceled -= m_Wrapper.m_MousePosActionsCallbackInterface.OnMouseMovement;
            }
            m_Wrapper.m_MousePosActionsCallbackInterface = instance;
            if (instance != null)
            {
                @MouseMovement.started += instance.OnMouseMovement;
                @MouseMovement.performed += instance.OnMouseMovement;
                @MouseMovement.canceled += instance.OnMouseMovement;
            }
        }
    }
    public MousePosActions @MousePos => new MousePosActions(this);
    public interface IMousePosActions
    {
        void OnMouseMovement(InputAction.CallbackContext context);
    }
}
