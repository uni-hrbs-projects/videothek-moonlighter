// GENERATED AUTOMATICALLY FROM 'Assets/Controls/2DInput.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @_2DInput : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @_2DInput()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""2DInput"",
    ""maps"": [
        {
            ""name"": ""Player"",
            ""id"": ""f7cd1012-0d5f-44df-bb05-05214f37320a"",
            ""actions"": [
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""c8cb9abd-46e6-44bd-9611-e01049269a73"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Interact"",
                    ""type"": ""Button"",
                    ""id"": ""10025bc4-501e-4dc8-b45a-17637deafb2b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Crouch"",
                    ""type"": ""Button"",
                    ""id"": ""eb338712-654d-4136-ae5c-fa437f665a9e"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Horizontal"",
                    ""type"": ""Button"",
                    ""id"": ""4a41b0a1-7e71-490b-a36d-86e40b2372aa"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""L1"",
                    ""type"": ""Button"",
                    ""id"": ""560e9262-4a20-4141-bd9e-9d04e149b10e"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""L2"",
                    ""type"": ""Button"",
                    ""id"": ""0255a5db-6e0f-4026-b8e4-eb3abcf67bc5"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""R1"",
                    ""type"": ""Button"",
                    ""id"": ""b6f8c0a9-a643-48bd-86e0-57960885d21f"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""R2"",
                    ""type"": ""Button"",
                    ""id"": ""127fdb64-6b6c-4278-b066-022ce9bb1dda"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Remote Throw"",
                    ""type"": ""Button"",
                    ""id"": ""d2bdc46b-2740-43bc-afd1-eee94220ff41"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Controller Use"",
                    ""type"": ""Button"",
                    ""id"": ""c69578de-07c9-40d0-9262-e1e67cd77813"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Mouse Use"",
                    ""type"": ""Button"",
                    ""id"": ""d3333566-9934-46e8-ace6-dc1f7fe1ba28"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""f1b3e401-1941-4da7-819f-135b4e64d3db"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f75aabc2-29ce-45b9-ab6e-3ae3ea84b210"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""32cecd4d-ec4f-467a-acde-b44e9b7d60f0"",
                    ""path"": ""<Keyboard>/leftCtrl"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Crouch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e7644755-c8ad-4aec-bc2f-253df0cbbab7"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Crouch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""1D Axis"",
                    ""id"": ""288b9b83-dfbf-4def-9aa5-6c8ce40e16c9"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Horizontal"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""b9a5caa0-7296-45e9-ae0c-6bfdbb5c224e"",
                    ""path"": ""<Gamepad>/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Horizontal"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""822e26db-1484-4ae5-afcd-f761a008c794"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Horizontal"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""13ef98f4-5ea5-481f-93db-aab00c249cb4"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Horizontal"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""9a4a62fd-f539-4d64-869a-3f5d52826769"",
                    ""path"": ""<Gamepad>/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Horizontal"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""31780cb1-0c80-4ad5-a768-4941cda3a479"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Horizontal"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""319641b3-d712-42b2-8015-5e2037f6e54b"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Horizontal"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""a49c8c3a-0bcf-451c-b22e-72ea0b6083b7"",
                    ""path"": ""<Gamepad>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""L1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""235dc2a9-308a-4839-b9fa-3cced597fc76"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""L1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ae02b62a-07c6-43de-a09e-32e4cfa3bbe1"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""L1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""729dd445-371e-4db3-b62d-84e33c7b1fac"",
                    ""path"": ""<Gamepad>/leftTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""L2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""29775357-82a4-4aea-aabc-a92ccac31904"",
                    ""path"": ""<Keyboard>/tab"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""L2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9a43b0eb-339c-45ed-a901-5a2a4b4718bb"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""R1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5911c834-5a4f-4212-9ccb-59a4586635af"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""R1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""503339c7-61e3-4fcd-ae23-d7e3d0e2ffd3"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""R1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8160907b-981b-4d93-b0fb-e539a7b59b0e"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""R2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""029c0d58-1f43-4f82-8268-c4ae569c2820"",
                    ""path"": ""<Keyboard>/r"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""R2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a07987d4-195c-4dde-a3c1-d6309476cdea"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e8ff2e31-c34a-4c30-b693-080b94479258"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2732d7e3-bf5d-4c14-9ed9-33da7d98243b"",
                    ""path"": ""<Keyboard>/shift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Remote Throw"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0dcc4e7f-9a7f-4864-a355-766ae1eac4a8"",
                    ""path"": ""<Gamepad>/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Controller Use"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""cb5bc982-45d7-4838-9449-47ec6b262a4e"",
                    ""path"": ""<Gamepad>/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Controller Use"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2dbc2413-da44-4db4-abab-c66204f8f2d3"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Mouse Use"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5dc0d23f-efe5-4b6b-8c0f-2cf24ab3f0e8"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Mouse Use"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Player
        m_Player = asset.FindActionMap("Player", throwIfNotFound: true);
        m_Player_Jump = m_Player.FindAction("Jump", throwIfNotFound: true);
        m_Player_Interact = m_Player.FindAction("Interact", throwIfNotFound: true);
        m_Player_Crouch = m_Player.FindAction("Crouch", throwIfNotFound: true);
        m_Player_Horizontal = m_Player.FindAction("Horizontal", throwIfNotFound: true);
        m_Player_L1 = m_Player.FindAction("L1", throwIfNotFound: true);
        m_Player_L2 = m_Player.FindAction("L2", throwIfNotFound: true);
        m_Player_R1 = m_Player.FindAction("R1", throwIfNotFound: true);
        m_Player_R2 = m_Player.FindAction("R2", throwIfNotFound: true);
        m_Player_RemoteThrow = m_Player.FindAction("Remote Throw", throwIfNotFound: true);
        m_Player_ControllerUse = m_Player.FindAction("Controller Use", throwIfNotFound: true);
        m_Player_MouseUse = m_Player.FindAction("Mouse Use", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Player
    private readonly InputActionMap m_Player;
    private IPlayerActions m_PlayerActionsCallbackInterface;
    private readonly InputAction m_Player_Jump;
    private readonly InputAction m_Player_Interact;
    private readonly InputAction m_Player_Crouch;
    private readonly InputAction m_Player_Horizontal;
    private readonly InputAction m_Player_L1;
    private readonly InputAction m_Player_L2;
    private readonly InputAction m_Player_R1;
    private readonly InputAction m_Player_R2;
    private readonly InputAction m_Player_RemoteThrow;
    private readonly InputAction m_Player_ControllerUse;
    private readonly InputAction m_Player_MouseUse;
    public struct PlayerActions
    {
        private @_2DInput m_Wrapper;
        public PlayerActions(@_2DInput wrapper) { m_Wrapper = wrapper; }
        public InputAction @Jump => m_Wrapper.m_Player_Jump;
        public InputAction @Interact => m_Wrapper.m_Player_Interact;
        public InputAction @Crouch => m_Wrapper.m_Player_Crouch;
        public InputAction @Horizontal => m_Wrapper.m_Player_Horizontal;
        public InputAction @L1 => m_Wrapper.m_Player_L1;
        public InputAction @L2 => m_Wrapper.m_Player_L2;
        public InputAction @R1 => m_Wrapper.m_Player_R1;
        public InputAction @R2 => m_Wrapper.m_Player_R2;
        public InputAction @RemoteThrow => m_Wrapper.m_Player_RemoteThrow;
        public InputAction @ControllerUse => m_Wrapper.m_Player_ControllerUse;
        public InputAction @MouseUse => m_Wrapper.m_Player_MouseUse;
        public InputActionMap Get() { return m_Wrapper.m_Player; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerActions instance)
        {
            if (m_Wrapper.m_PlayerActionsCallbackInterface != null)
            {
                @Jump.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJump;
                @Jump.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJump;
                @Jump.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJump;
                @Interact.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnInteract;
                @Interact.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnInteract;
                @Interact.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnInteract;
                @Crouch.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnCrouch;
                @Crouch.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnCrouch;
                @Crouch.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnCrouch;
                @Horizontal.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnHorizontal;
                @Horizontal.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnHorizontal;
                @Horizontal.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnHorizontal;
                @L1.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnL1;
                @L1.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnL1;
                @L1.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnL1;
                @L2.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnL2;
                @L2.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnL2;
                @L2.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnL2;
                @R1.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnR1;
                @R1.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnR1;
                @R1.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnR1;
                @R2.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnR2;
                @R2.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnR2;
                @R2.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnR2;
                @RemoteThrow.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnRemoteThrow;
                @RemoteThrow.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnRemoteThrow;
                @RemoteThrow.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnRemoteThrow;
                @ControllerUse.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnControllerUse;
                @ControllerUse.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnControllerUse;
                @ControllerUse.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnControllerUse;
                @MouseUse.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMouseUse;
                @MouseUse.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMouseUse;
                @MouseUse.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMouseUse;
            }
            m_Wrapper.m_PlayerActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Jump.started += instance.OnJump;
                @Jump.performed += instance.OnJump;
                @Jump.canceled += instance.OnJump;
                @Interact.started += instance.OnInteract;
                @Interact.performed += instance.OnInteract;
                @Interact.canceled += instance.OnInteract;
                @Crouch.started += instance.OnCrouch;
                @Crouch.performed += instance.OnCrouch;
                @Crouch.canceled += instance.OnCrouch;
                @Horizontal.started += instance.OnHorizontal;
                @Horizontal.performed += instance.OnHorizontal;
                @Horizontal.canceled += instance.OnHorizontal;
                @L1.started += instance.OnL1;
                @L1.performed += instance.OnL1;
                @L1.canceled += instance.OnL1;
                @L2.started += instance.OnL2;
                @L2.performed += instance.OnL2;
                @L2.canceled += instance.OnL2;
                @R1.started += instance.OnR1;
                @R1.performed += instance.OnR1;
                @R1.canceled += instance.OnR1;
                @R2.started += instance.OnR2;
                @R2.performed += instance.OnR2;
                @R2.canceled += instance.OnR2;
                @RemoteThrow.started += instance.OnRemoteThrow;
                @RemoteThrow.performed += instance.OnRemoteThrow;
                @RemoteThrow.canceled += instance.OnRemoteThrow;
                @ControllerUse.started += instance.OnControllerUse;
                @ControllerUse.performed += instance.OnControllerUse;
                @ControllerUse.canceled += instance.OnControllerUse;
                @MouseUse.started += instance.OnMouseUse;
                @MouseUse.performed += instance.OnMouseUse;
                @MouseUse.canceled += instance.OnMouseUse;
            }
        }
    }
    public PlayerActions @Player => new PlayerActions(this);
    public interface IPlayerActions
    {
        void OnJump(InputAction.CallbackContext context);
        void OnInteract(InputAction.CallbackContext context);
        void OnCrouch(InputAction.CallbackContext context);
        void OnHorizontal(InputAction.CallbackContext context);
        void OnL1(InputAction.CallbackContext context);
        void OnL2(InputAction.CallbackContext context);
        void OnR1(InputAction.CallbackContext context);
        void OnR2(InputAction.CallbackContext context);
        void OnRemoteThrow(InputAction.CallbackContext context);
        void OnControllerUse(InputAction.CallbackContext context);
        void OnMouseUse(InputAction.CallbackContext context);
    }
}
